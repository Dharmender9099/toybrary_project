package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dharmender on 07-11-2017.
 */

public class RegisterFragment  extends Fragment{

    EditText firstname,lastname,password,confirmpassword,mobile,email;
    Button register;
    String user_id,res;
    ProgressDialog progressDialog;
    Context context;
    String errormsg="";
    private WifiManager wifiManager;
    private WifiInfo info;
    String device_address;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.register_fragment,container,false);
        // get unique id of user
        wifiManager= (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        info = wifiManager.getConnectionInfo();
        device_address=info.getMacAddress().toUpperCase();

        firstname=(EditText)view.findViewById(R.id.firstname);
        lastname=(EditText)view.findViewById(R.id.lastname);
        password=(EditText)view.findViewById(R.id.password);
        confirmpassword=(EditText)view.findViewById(R.id.confirmpassword);
        mobile=(EditText)view.findViewById(R.id.mobile);
        email=(EditText)view.findViewById(R.id.email);
        register=(Button)view.findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userRegistration();
            }
        });

        return view;
    }
    private void userRegistration()
    {

        final String user_firstname=firstname.getText().toString();
        final String user_lastname=lastname.getText().toString();
        final String user_password=password.getText().toString();
        final String user_confirmpassword=confirmpassword.getText().toString();
        final String user_mobile=mobile.getText().toString();
        final String user_emailid=email.getText().toString();

        if (TextUtils.isEmpty(user_firstname)) {
            firstname.setError("Please enter username");
            firstname.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(user_lastname))
        {
            lastname.setError("please enter lastname");
            lastname.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(user_password))
        {
            password.setError("please enter password");
            password.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(user_confirmpassword))
        {
            confirmpassword.setError("please enter Confirm Password");
            confirmpassword.requestFocus();
            return;
        }
        if(!user_password.equals(user_confirmpassword))
        {
            confirmpassword.setError("password does not match");
            confirmpassword.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(user_mobile))
        {
            mobile.setError("please enter Mobile no");
            mobile.requestFocus();
            return;
        }
        if(user_mobile.length()>10)
        {
            mobile.setError("please enter 10 digit Mobile no");
            mobile.requestFocus();
            return;
        }
        if(user_mobile.length()<10)
        {
            mobile.setError("please enter valied mobile no");
            mobile.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(user_emailid))
        {
            email.setError("please enter email");
            email.requestFocus();
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(user_emailid).matches()) {
            email.setError("Enter a valid email");
            email.requestFocus();
            return;
        }
        progressDialog = ProgressDialog.show(getContext(), "", "Please wait..");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.REGISTER_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                if (response.trim().equals("user already registered.please change email or mobileno")) {
                    Toast.makeText(getActivity(), response.trim(), Toast.LENGTH_LONG).show();
                }
                else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        JSONArray packages = jsonObject.getJSONArray("user_detail");
                        for (int i = 0; i < packages.length(); i++) {
                            JSONObject obj1 = packages.getJSONObject(i);
                            res = obj1.getString("msg");
                            user_id = obj1.getString("user_id");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                        if (res.equals("user successfully registered")) {
                            SessionManager sessionManager=new SessionManager(getActivity());
                            sessionManager.createLoginSession(user_firstname,user_id);
                            Toast.makeText(getContext(), res, Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().finish();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if( error instanceof NetworkError) {
                            errormsg="Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof AuthFailureError) {
                            errormsg="Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof ServerError) {
                            errormsg="The server could not be found. Please try again after some time!!";
                        }
                        else if (error instanceof ParseError) {
                            errormsg="Parsing error! Please try again after some time!!";
                        }
                        else if (error instanceof NoConnectionError) {
                            errormsg = "Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof TimeoutError) {
                            errormsg = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getActivity(),errormsg,Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("firstname",user_firstname);
                params.put("lastname",user_lastname);
                params.put("password", user_confirmpassword);
                params.put("mobile",user_mobile);
                params.put("email",user_emailid);
                params.put("user_track_id",device_address);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

}
