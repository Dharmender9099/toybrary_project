package com.toybraryapp.childs.toybrary.util;

import android.content.Context;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.activities.AppController;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dharmender on 07-12-2017.
 */

public class CartItem{
 Context context;
    int totalitem;
 public CartItem(Context context)
 {
     this.context=context;
 }
    public int totalItem()
    {

        StringRequest stringRequest=new StringRequest(StringRequest.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                    String res=response.trim();
                    // totalitem =Integer.parseInt(res);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,String.valueOf(error),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                SessionManager sessionManager=new SessionManager(context);
                String userId=sessionManager.getUserid();
                Map<String,String> params=new HashMap<>();
                params.put("user_id",userId);
                params.put("action","total_cart_item");
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
        return totalitem;
    }

}
