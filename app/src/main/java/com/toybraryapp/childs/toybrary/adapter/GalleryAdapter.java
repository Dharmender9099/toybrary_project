package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.toybraryapp.childs.toybrary.activities.AppController;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.Gallery;
import java.util.List;

/**
 * Created by Dharmender on 05-11-2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryHolder> {
    private Context context;
    private List<Gallery> galleryList;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public GalleryAdapter(Context context,List<Gallery> galleryList)
    {
        this.context=context;
        this.galleryList=galleryList;
    }
    @Override
    public GalleryAdapter.GalleryHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
      LayoutInflater layoutInflater=LayoutInflater.from(context);
      View view= layoutInflater.inflate(R.layout.gallery_view_layout,null);
        return new GalleryHolder(view);
    }

    @Override
    public void onBindViewHolder(GalleryHolder galleryHolder, int position) {
      Gallery gallery=galleryList.get(position);
       /* Glide.with(context)
                .load(gallery.getImage())
                .into(galleryHolder.galleryImage);*/
       galleryHolder.galleryImage.setImageUrl(gallery.getImage(),imageLoader);
         galleryHolder.imageName.setText(gallery.getName());
    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }

    public class GalleryHolder extends RecyclerView.ViewHolder
    {
     NetworkImageView galleryImage;
     TextView imageName;
        public GalleryHolder(View itemView) {
            super(itemView);
            galleryImage=(NetworkImageView)itemView.findViewById(R.id.galleryImage);
            imageName=(TextView)itemView.findViewById(R.id.imageName);
        }
    }
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private GalleryAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final GalleryAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
