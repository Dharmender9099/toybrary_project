package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.activities.URLVideoActivity;
import com.toybraryapp.childs.toybrary.model.Video;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.List;

/**
 * Created by Dharmender on 01-01-2018.
 */

public class VideoAdapter0 extends RecyclerView.Adapter {
    Context context;
    List<Video> mvideo;
    DisplayMetrics dm;
    SurfaceView sur_View;
    MediaController media_Controller;
    public VideoAdapter0(Context context, List<Video> mvideo)
    {
        this.context=context;
        this.mvideo=mvideo;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.video_layout,null);
       // View view= LayoutInflater.from(context).inflate(R.layout.video_layout,parent,false);
        return new VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
     final Video video=mvideo.get(position);
     final VideoHolder videoHolder=(VideoHolder) holder;
        try {
            //play video using android api, when video view is clicked.
            String videoImg=video.getVideoImage();
            String videoTit=video.getVideoTitle();
            videoHolder.videoName.setText(videoTit);
            Uri videoUri = Uri.parse(URLs.VIDEO_PATH+videoImg);
            videoHolder.videoView.setVideoURI(videoUri);
            videoHolder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                    mp.setVolume(0,0);
                    media_Controller = new MediaController(context);
                   /* dm = new DisplayMetrics();
                    int height = dm.heightPixels;
                    int width = dm.widthPixels;
                    videoHolder.videoView.setMinimumWidth(width);
                    videoHolder.videoView.setMinimumHeight(height);*/
                    //videoHolder.videoView.setMediaController(media_Controller);
                    videoHolder.videoView.start();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        videoHolder.video_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                  //
                //  Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLs.VIDEO_PATH + video.getVideoImage()));
                Intent intent=new Intent(context,URLVideoActivity.class);
                intent.putExtra("uri",URLs.VIDEO_PATH+video.getVideoImage());
                    context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mvideo.size();
    }
    public class VideoHolder extends RecyclerView.ViewHolder
    {
        VideoView videoView;
        TextView videoName;
        LinearLayout video_layout;
        public VideoHolder(View itemView) {
            super(itemView);
            videoView=(VideoView)itemView.findViewById(R.id.videoView);
            videoName=(TextView)itemView.findViewById(R.id.videoName);
            video_layout=(LinearLayout)itemView.findViewById(R.id.video_layout);
        }
    }
}
