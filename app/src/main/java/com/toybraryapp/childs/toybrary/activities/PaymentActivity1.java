package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

public class PaymentActivity1 extends AppCompatActivity implements View.OnClickListener{
    String userid;
    LinearLayout cod,coq,online;
    TextView txt_cod,txt_coq,txt_paytm;
    CheckBox checkbox;
    LinearLayout linearLayout;
    String alldetails[];
    ProgressDialog progressDialog;
    String paymentmethod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment1);
        getSupportActionBar().setTitle("Payment Mode");
        SessionManager sessionManager=new SessionManager(this);
        txt_cod=(TextView)findViewById(R.id.txt_cod);
        txt_coq=(TextView)findViewById(R.id.txt_coq);
        txt_paytm=(TextView)findViewById(R.id.txt_paytm);
        checkbox=(CheckBox)findViewById(R.id.checkbox);
        cod=(LinearLayout)findViewById(R.id.cod);
        coq=(LinearLayout)findViewById(R.id.coq);
        online=(LinearLayout)findViewById(R.id.online);
        alldetails=getIntent().getStringArrayExtra("user_details");
        userid=sessionManager.getUserid();
        cod.setOnClickListener(this);
        coq.setOnClickListener(this);
        online.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.cod:
             if(checkbox.isChecked())
             {
                 purchasePackage();
                 paymentmethod=txt_cod.getText().toString();
             }
             else
             {
             getSnackbar();
             }
                break;
            case R.id.coq:
                if(checkbox.isChecked())
                {
                       purchasePackage();
                       paymentmethod=txt_coq.getText().toString();
                }
                else
                {
             getSnackbar();
                }
                break;
            case R.id.online:
                if(checkbox.isChecked())
                {
                    startActivity(new Intent(PaymentActivity1.this,MerchantActivity.class));
                  // purchasePackage();
                   //paymentmethod=txt_paytm.getText().toString();
                }
                else
                {
             getSnackbar();
                }
                break;
        }
    }
    public void getSnackbar()
    {
        linearLayout=(LinearLayout)findViewById(R.id.linear_layout);
        Snackbar snackbar=Snackbar.make(linearLayout,"Please accept terms & condition",Snackbar.LENGTH_LONG);
        View view2=snackbar.getView();
        TextView textView = (TextView) view2.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
       // FrameLayout.LayoutParams params= (FrameLayout.LayoutParams) view2.getLayoutParams();
      //  params.gravity= Gravity.TOP;
       // view2.setLayoutParams(params);
        snackbar.show();
    }

    public void purchasePackage()
    {
        progressDialog = ProgressDialog.show(this, "", "Please wait...");
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URLs.SHIPING_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                String res=response.trim();
                if(res.equals("your package request successfully registered"))
                {
                    Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
                    startActivity(new Intent(PaymentActivity1.this,Package_ThankyouActivity.class));
                }
                else {
                    Toast.makeText(getApplication(),response,Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplication(),String.valueOf(error),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap();
                params.put("userid",userid);
                params.put("packageid",alldetails[0]);
                params.put("promocode",alldetails[1]);
                params.put("depositamount",alldetails[2]);
                params.put("billingaddress",alldetails[3]);
                params.put("billingcountry",alldetails[4]);
                params.put("billingstate",alldetails[5]);
                params.put("billingcity",alldetails[6]);
                params.put("billingpostalcode",alldetails[7]);
                params.put("shipingaddress",alldetails[8]);
                params.put("shipingcity",alldetails[9]);
                params.put("shipingpostalcode",alldetails[10]);
                params.put("shipingstate",alldetails[11]);
                params.put("shipingcountry",alldetails[12]);
                params.put("childname",alldetails[13]);
                params.put("childbirthdate",alldetails[14]);
                params.put("payment_option",paymentmethod);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
