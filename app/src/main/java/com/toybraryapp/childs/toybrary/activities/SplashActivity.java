package com.toybraryapp.childs.toybrary.activities;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.RemoteException;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.internet.CheckInternetConnection;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

import static com.android.installreferrer.api.InstallReferrerClient.newBuilder;

public class SplashActivity extends AppCompatActivity implements InstallReferrerStateListener {
    LinearLayout linearLayout;
    private WifiManager wifiManager;
    private WifiInfo info;
    String device_address;
    String userid;
    String referralUser;
    SessionManager sessionManager;
    private String TAG = SplashActivity.class.getSimpleName();
    private InstallReferrerClient mReferrerClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sessionManager=new SessionManager(SplashActivity.this);
        userid=sessionManager.getUserid();
        // get unique id of user

        wifiManager= (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        info = wifiManager.getConnectionInfo();
        device_address=info.getMacAddress().toUpperCase();

        // establish the connection between your app and play store

        mReferrerClient = newBuilder(this).build();
        mReferrerClient.startConnection(this);

        linearLayout=(LinearLayout)findViewById(R.id.interneterror);
        CheckInternetConnection checkInternetConnection = new CheckInternetConnection(this);
        Boolean result = checkInternetConnection.isConnectTointernet();
        if (result==true) {
            Handler obj = new Handler();
            obj.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Boolean islogin=sessionManager.isLogin();
                   if(islogin) {
                   startActivity(new Intent(SplashActivity.this,MainActivity.class));
                   finish();
                   }
                   else {
                       startActivity(new Intent(SplashActivity.this, FragmentActivity.class));
                       finish();
                   }
                }
            }, 1000);
        }
       else
       {
        /* linearLayout.setVisibility(View.VISIBLE);
         ok.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 startActivity(new Intent(SplashActivity.this,MainActivity.class));
                 finish();
             }
         });*/
           LayoutInflater layoutInflater=LayoutInflater.from(this);
           View view=layoutInflater.inflate(R.layout.no_internet_connection_layout,null);
           AlertDialog.Builder builder=new AlertDialog.Builder(this);
           builder.setView(view);
           Button ok=(Button)view.findViewById(R.id.ok);
           ok.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   startActivity(new Intent(SplashActivity.this,MainActivity.class));
                   finish();
               }
           });
           AlertDialog alertDialog=builder.create();
           alertDialog.show();
       }
    }

    // implement install referrer API

    @Override
    public void onInstallReferrerSetupFinished(int responseCode) {
        switch (responseCode) {
            case InstallReferrerClient.InstallReferrerResponse.OK:
                try {
                    Log.v(TAG, "InstallReferrer conneceted");
                    ReferrerDetails response = mReferrerClient.getInstallReferrer();
                    referralUser=response.getInstallReferrer();
                    if(!referralUser.isEmpty())
                    {
                        trackUser();
                    }
                    //handleReferrer(response);
                    mReferrerClient.endConnection();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                Log.w(TAG, "InstallReferrer not supported");
                break;
            case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                Log.w(TAG, "Unable to connect to the service");
                break;
            default:
                Log.w(TAG, "responseCode not found.");
        }
    }

    @Override
    public void onInstallReferrerServiceDisconnected() {

    }
    // track referrer user
    public void trackUser()
    {
        StringRequest stringRequest=new StringRequest(StringRequest.Method.POST, URLs.TRACKUSER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplication(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("promoter_id",referralUser);
                map.put("user_id",userid);
                map.put("user_track_id",device_address);
                return map;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
