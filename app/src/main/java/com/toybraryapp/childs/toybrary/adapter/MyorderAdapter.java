package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.toybraryapp.childs.toybrary.activities.OrderDetailsActivity;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.activities.ReviewActivity;
import com.toybraryapp.childs.toybrary.model.Gallery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dharmender on 15-01-2018.
 */

public class MyorderAdapter extends RecyclerView.Adapter<MyorderAdapter.MyOrderHolder>{
    List<Gallery> list=new ArrayList<>();
    Context context;
    public MyorderAdapter(Context context,List<Gallery> list)
    {
       this.context=context;
       this.list=list;
    }

    @Override
    public MyOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       LayoutInflater layoutInflater= LayoutInflater.from(context);
       View view=layoutInflater.inflate(R.layout.myorder_layout,null);
        return new MyorderAdapter.MyOrderHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyOrderHolder holder, int position) {
    Gallery gallery=list.get(position);
    holder.name.setText(gallery.getName());
    holder.deliverdate.setText(gallery.getDeliverdate());
    holder.orderid.setText(gallery.getId());
    holder.productid.setText(gallery.getProductid());
    Glide.with(context).load(gallery.getImage()).into(holder.productimage);
    holder.ordercardview.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String oid=holder.orderid.getText().toString();
            Intent intent=new Intent(context,OrderDetailsActivity.class);
            intent.putExtra("orderid",oid);
            intent.putExtra("productid",holder.productid.getText().toString());
            context.startActivity(intent);
        }
    });
    holder.review.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        Intent intent=new Intent(context, ReviewActivity.class);
        intent.putExtra("productid",holder.productid.getText().toString());
        context.startActivity(intent);
        }
    });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyOrderHolder extends RecyclerView.ViewHolder {
        TextView name,deliverdate,orderid,review,productid;
        ImageView productimage;
        CardView ordercardview;
        public MyOrderHolder(View itemView) {
            super(itemView);
            orderid=(TextView)itemView.findViewById(R.id.orderid);
            name=(TextView)itemView.findViewById(R.id.name);
            deliverdate=(TextView)itemView.findViewById(R.id.deliverdate);
            productimage=(ImageView)itemView.findViewById(R.id.productimage);
            ordercardview=(CardView)itemView.findViewById(R.id.order_cardview);
            review=(TextView)itemView.findViewById(R.id.review);
            productid=(TextView)itemView.findViewById(R.id.productid);
        }
    }

}
