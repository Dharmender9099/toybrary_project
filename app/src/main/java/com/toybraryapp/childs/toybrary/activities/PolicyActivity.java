package com.toybraryapp.childs.toybrary.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.toybraryapp.childs.toybrary.R;

public class PolicyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);
        getSupportActionBar().setTitle("Privacy and Policy");
    }
}
