package com.toybraryapp.childs.toybrary.activities;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.CartAdapter;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.PointsCounter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class ActivityMy_Cart extends AppCompatActivity {
RecyclerView cartitem;
RecyclerView.LayoutManager layoutManager;
List<Favourites> cartlist;
String errormsg="";
String user_id;
int points=0;
int count=0;
int ps=0;
TextView totalpoints,availablepoints,place_order;
    private PointsCounter counter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__cart);
        ActionBar actionBar = getSupportActionBar(); // or getActionBar();
        getSupportActionBar().setTitle("My Cart"); // set the top title
        //String title = actionBar.getTitle().toString(); // get the title
       // actionBar.hide();

        SessionManager sessionManager=new SessionManager(this);
        totalpoints=(TextView)findViewById(R.id.tpoints);
        place_order=(TextView)findViewById(R.id.place_order);
        availablepoints=(TextView)findViewById(R.id.availablepoints);
        user_id=sessionManager.getUserid();
        cartitem=(RecyclerView)findViewById(R.id.cartItemRecyclerview);
        layoutManager=new LinearLayoutManager(this);
        if(user_id.isEmpty())
        {
         Toast.makeText(getApplicationContext(),"please purchase item first.",Toast.LENGTH_LONG).show();
        }
        else {
            showData();
        }
        counter=new PointsCounter() {
            @Override
            public void setpointCounter(int count, String action) {
                String ponts = totalpoints.getText().toString();
                int n = (int) Double.parseDouble(ponts);
                if(action.equals("add")) {
                    n = n + count;
                    totalpoints.setText(String.valueOf(n));
                }
                else if(action.equals("remove"))
                {
                    n = n-count;
                    totalpoints.setText(String.valueOf(n));
                }
                else if(action.equals("deleteItem"))
                {
                    n=n-count;
                    totalpoints.setText(String.valueOf(n));
                }
            }
        };
        place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ava_points=Integer.parseInt(availablepoints.getText().toString());
                int t_points=Integer.parseInt(totalpoints.getText().toString());
                if(t_points<=ava_points) {
                    Intent intent = new Intent(ActivityMy_Cart.this, CheckOutActivity.class);
                    intent.putExtra("available_point",String.valueOf(ava_points));
                    intent.putExtra("cart_totalpoint", totalpoints.getText().toString());
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplication(),"Not enough points",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ActivityMy_Cart.this,PackagesActivity.class));
                }
            }
        });
    }

    public void showData()
    {
        StringRequest stringRequest=new StringRequest(StringRequest.Method.POST, URLs.VIEW_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                cartlist=new ArrayList<>();
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("cartItem");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Favourites favourites = new Favourites();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    favourites.setId(obj.getString("productId"));
                    favourites.setImage(URLs.SEARCH_IMAGE_PATH + obj.getString("productPhoto"));
                    favourites.setName(obj.getString("productName"));
                    favourites.setPoints(obj.getString("productPoints"));
                    favourites.setQuantity(obj.getString("productQuantity"));
                    favourites.setTotalpoints(obj.getString("productTotalpoints"));
                    String tpoints = obj.getString("productTotalpoints");
                    points = (int) (points + Double.parseDouble(tpoints));
                    cartlist.add(favourites);
                }


                    totalpoints.setText(String.valueOf(points));
                JSONObject jsonObject1 = new JSONObject(response);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("points");
                JSONObject obj1 = jsonArray1.getJSONObject(0);
                availablepoints.setText(obj1.getString("points"));
            }
            catch(JSONException e)
            {
             e.printStackTrace();
            }
                CartAdapter cartAdapter=new CartAdapter(ActivityMy_Cart.this,cartlist,counter);
                cartitem.setLayoutManager(layoutManager);
                cartitem.setAdapter(cartAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error instanceof TimeoutError || error instanceof NoConnectionError) {
                    errormsg="No internet connection.";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Authentication Failure.";
                }
                else if (error instanceof ServerError) {
                    errormsg="Server down.";
                }
                else if (error instanceof ParseError) {
                    errormsg="problem in fetching data.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("user_id",user_id);
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }


}
