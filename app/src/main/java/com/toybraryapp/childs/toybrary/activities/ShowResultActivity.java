package com.toybraryapp.childs.toybrary.activities;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.GridAdapter;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.Counter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ShowResultActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    RecyclerView searchRecyclerView;
    List<Favourites> searchItem;
    GridLayoutManager manager;
    String New_url;
    TextView itemCounts;
    LinearLayout linearLayout;
    Counter counter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);
        linearLayout=(LinearLayout)findViewById(R.id.linear_layout);
        progressBar = (ProgressBar) findViewById(R.id.progresscycle);
        itemCounts=(TextView)findViewById(R.id.itemsCount);
        searchRecyclerView=(RecyclerView)findViewById(R.id.searchRecyclerView);
        manager = new GridLayoutManager(this, 2);
        String value= getIntent().getStringExtra("productname");
        getSupportActionBar().setTitle(value);
        New_url= URLs.SEARCH_URL+value;
        searchItem();
        counter=new Counter() {
            @Override
            public void setCounter(int count) {

            }
        };
    }

    public void searchItem()
    {
        progressBar.setVisibility(View.VISIBLE);
        JsonArrayRequest searchReq = new JsonArrayRequest(New_url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBar.setVisibility(View.GONE);
                        searchItem=new ArrayList<>();
                        for (int i = 0; i < response.length(); i++) {

                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Favourites favourites=new Favourites();
                                favourites.setId(obj.getString("productId"));
                                favourites.setImage(URLs.SEARCH_IMAGE_PATH+obj.getString("productPhoto"));
                                favourites.setName(obj.getString("imagetitle"));
                                favourites.setNumber(obj.getString("productPoints"));
                                favourites.setStock(obj.getString("productStatus"));
                                searchItem.add(favourites);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        // notifying list adapter about data changes
                        if(searchItem.size()>0) {
                            itemCounts.setText(String.valueOf(searchItem.size()));
                            GridAdapter gridAdapter = new GridAdapter(ShowResultActivity.this, searchItem, counter);
                            searchRecyclerView.setItemAnimator(new DefaultItemAnimator());
                            searchRecyclerView.setLayoutManager(manager);
                            searchRecyclerView.setAdapter(gridAdapter);
                            gridAdapter.notifyDataSetChanged();
                        }
                        else
                        {
                            Snackbar snackbar = Snackbar.make(linearLayout, "Sorry no record found!", Snackbar.LENGTH_LONG);
                            View sbView = snackbar.getView();
                            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(Color.YELLOW);
                            snackbar.show();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),String.valueOf(error.getMessage()),Toast.LENGTH_LONG).show();

            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(searchReq);
    }
}
