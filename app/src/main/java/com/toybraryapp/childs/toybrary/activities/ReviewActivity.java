package com.toybraryapp.childs.toybrary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;
public class ReviewActivity extends AppCompatActivity {
      String userid,errormsg,productid;
      EditText title,review;
      RatingBar ratingbar;
      Button btnreview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        getSupportActionBar().setTitle("Review");
        title=(EditText)findViewById(R.id.title);
        review=(EditText)findViewById(R.id.review);
        btnreview=(Button)findViewById(R.id.btn_review);
        ratingbar=(RatingBar)findViewById(R.id.rating);
        productid=getIntent().getStringExtra("productid");
        SessionManager sessionManager=new SessionManager(this);
        userid=sessionManager.getUserid();
        btnreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             userReview();
            }
        });
    }
    public void userReview()
    {
        final String rating=String.valueOf(ratingbar.getRating());
        final String review_title=title.getText().toString();
        final String query=review.getText().toString();
        if(review_title.isEmpty())
        {
            title.setError("please enter title");
            title.requestFocus();
            return;
        }
        if(query.isEmpty())
        {
            review.setError("please enter any query");
            review.requestFocus();
            return;
        }
        StringRequest reviewReq=new StringRequest(StringRequest.Method.POST, URLs.REVIEW_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                if(res.equals("Thanks for your review and rating!"))
                {
                    Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ReviewActivity.this,MainActivity.class));
                }
                else
                {
                    Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("userid",userid);
                map.put("title",review_title);
                map.put("review",query);
                map.put("productid",productid);
                map.put("rating",rating);
                return map;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(reviewReq);
    }
}
