package com.toybraryapp.childs.toybrary.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.activities.SingleProductActivity;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.OnLoadMoreListener;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.Counter;
import com.toybraryapp.childs.toybrary.volleyRequest.JsonParserVolley;
import com.toybraryapp.childs.toybrary.volleyRequest.VolleyResponse;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dharmender on 19-02-2018.
 */

public class AllCategoryAdapter extends RecyclerView.Adapter{
    List<Favourites> list=new ArrayList<>();
    private Activity activity;
    private boolean isLoading;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    Counter counter;
    public AllCategoryAdapter(RecyclerView recyclerView, Activity activity, List<Favourites> list,Counter counter)
    {
        this.activity=activity;
        this.list=list;
        this.counter=counter;
       if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position)== null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.allcategory_layout, parent, false);
            vh = new AllCategoryHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false);
            vh = new AllCategoryAdapter.LoadingViewHolder(view);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AllCategoryHolder) {
            Favourites bean = list.get(position);
            final AllCategoryHolder allCategoryHolder = (AllCategoryHolder) holder;
            allCategoryHolder.id.setText(bean.getId());
            Glide.with(activity).load(bean.getImage()).into(allCategoryHolder.productImage);
            allCategoryHolder.name.setText(bean.getName());
            allCategoryHolder.points.setText(bean.getPoints());
            allCategoryHolder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(activity, SingleProductActivity.class);
                    String value=allCategoryHolder.id.getText().toString();
                    intent.putExtra("productId",value);
                    activity.startActivity(intent);
                }
            });
            allCategoryHolder.productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(activity, SingleProductActivity.class);
                    String value=allCategoryHolder.id.getText().toString();
                    intent.putExtra("productId",value);
                    activity.startActivity(intent);
                }
            });
            allCategoryHolder.addtocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SessionManager sessionManager=new SessionManager(activity);
                    String userId=sessionManager.getUserid();
                        if (!userId.isEmpty()) {
                            String productId = allCategoryHolder.id.getText().toString();
                            String add = "additem";
                            updateCart(productId, userId, add);
                        } else {
                            Toast.makeText(activity, "please register first!", Toast.LENGTH_LONG).show();
                        }

                }
            });
        } else if (holder instanceof AllCategoryAdapter.LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }
    public void updateCart(String productid,String userid,String action)
    {
        JsonParserVolley jsonParserVolley=new JsonParserVolley(activity);
        jsonParserVolley.addHeader("product_id",productid);
        jsonParserVolley.addHeader("user_id",userid);
        jsonParserVolley.addHeader("action",action);
        jsonParserVolley.executeRequest(Request.Method.POST, URLs.UPDATE_CART, new JsonParserVolley.VolleyCallback() {
            @Override
            public void getResponse(String response) {
             Log.d("Response",response.toString());
             String res=response.trim();
                Toast.makeText(activity,res,Toast.LENGTH_LONG).show();
                if(res.equals("product successfully added in cart")) {
                    counter.setCounter(1);
                }
            }

            @Override
            public void getError(VolleyError error) {
             Log.d("Error",error.getMessage().toString());
            }
        });

    }
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
  class LoadingViewHolder extends RecyclerView.ViewHolder {

      public ProgressBar progressBar;

      public LoadingViewHolder(View view) {
          super(view);
          progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
      }
    }
    class AllCategoryHolder extends RecyclerView.ViewHolder {
        ImageView productImage,addtocart;
        TextView name,points,id;
        public AllCategoryHolder(View itemView) {
            super(itemView);
            id=(TextView)itemView.findViewById(R.id.itemId);
            productImage=(ImageView)itemView.findViewById(R.id.imageView);
            addtocart=(ImageView)itemView.findViewById(R.id.addtocart);
            name=(TextView)itemView.findViewById(R.id.name);
            points=(TextView)itemView.findViewById(R.id.points);
        }
    }

    public void setLoaded() {
        isLoading = false;
    }
}
