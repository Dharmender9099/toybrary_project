package com.toybraryapp.childs.toybrary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.toybraryapp.childs.toybrary.R;

public class ThankYouActivity extends AppCompatActivity implements View.OnClickListener{
    Button next;
    TextView txndate,txnamount,txnid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        init();
        setAction();
    }
    public void init()
    {
        txndate=(TextView)findViewById(R.id.txndate);
        txndate.setText(getIntent().getStringExtra("txndate"));
        txnamount=(TextView)findViewById(R.id.txnamount);
        txnamount.setText(getIntent().getStringExtra("txnamount"));
        txnid=(TextView)findViewById(R.id.txnid);
        txnid.setText(getIntent().getStringExtra("txnid"));
        next=(Button)findViewById(R.id.next);
    }
    public void setAction()
    {
      next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
      switch(view.getId())
      {
          case R.id.next:
              startActivity(new Intent(ThankYouActivity.this,MainActivity.class));
              break;
      }
    }
}
