package com.toybraryapp.childs.toybrary.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.toybraryapp.childs.toybrary.activities.CategoryItemViewActivity;
import com.toybraryapp.childs.toybrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dharmender on 02-11-2017.
 */

public class ListAdapter extends BaseAdapter {
    List<String> list=new ArrayList<>();
    Context context;
    ViewItem viewItem=null;
    public ListAdapter(Context context,List<String> list)
    {
        this.context=context;
        this.list=list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if(view==null)
        {
            viewItem=new ViewItem();
            LayoutInflater layoutInfiater = (LayoutInflater)this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = layoutInfiater.inflate(R.layout.view_layout, null);
            viewItem.product_id=(TextView)view.findViewById(R.id.product_id);
            viewItem.item=(TextView)view.findViewById(R.id.item);
            view.setTag(viewItem);

        }
        else
        {
           viewItem=(ViewItem)view.getTag();
        }
        viewItem.product_id.setText(list.get(position));
        viewItem.item.setText(list.get(position));
        viewItem.product_id.setVisibility(View.INVISIBLE);
        viewItem.product_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selectItemId= viewItem.product_id.toString();
                Intent intent=new Intent(context,CategoryItemViewActivity.class);
                intent.putExtra("getId",selectItemId);
                intent.putExtra("getFilterName","brands");
                context.startActivity(intent);
            }
        });
        return view;
    }
}
class ViewItem
{
    TextView item,product_id;

}