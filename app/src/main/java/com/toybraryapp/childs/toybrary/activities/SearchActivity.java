package com.toybraryapp.childs.toybrary.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends AppCompatActivity {
    EditText search_editText;
    TextView txtViewCount;
    int n;
    String errormsg = "";

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Search");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        getCartTotalItem();
        invalidateOptionsMenu();

        search_editText = (EditText) findViewById(R.id.search_editTextView);
        search_editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    Intent intent = new Intent(SearchActivity.this, ShowResultActivity.class);
                    intent.putExtra("productname", textView.getText().toString());
                    startActivity(intent);
                    finish();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.addcart);
        MenuItemCompat.setActionView(menuItem, R.layout.cart_icon_toolbar);
        RelativeLayout mycarttoolbar = (RelativeLayout) MenuItemCompat.getActionView(menuItem);
        RelativeLayout relativeLayout = (RelativeLayout) mycarttoolbar.findViewById(R.id.relative_layout_item_count);
        txtViewCount = (TextView) mycarttoolbar.findViewById(R.id.badge_notification_1);
        if (n != 0) {
            txtViewCount.setVisibility(View.VISIBLE);
            txtViewCount.setText(String.valueOf(n));
        }
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SearchActivity.this, ActivityMy_Cart.class));
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.contactus) {
            startActivity(new Intent(this, ContactUsActivity.class));
        } else if (id == R.id.logout) {
            SharedPreferences preferences =getSharedPreferences("myPref1",MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
            finish();
        }
        else if(id==R.id.editprofile)
        {
            startActivity(new Intent(this,EditProfileActivity.class));
        }
        else if(id==R.id.changepass)
        {
            startActivity(new Intent(this,ChangePasswordActivity.class));
        }
        else if(id==R.id.privacy)
        {
            startActivity(new Intent(this,PolicyActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }

    private void performSearch() {
        search_editText.clearFocus();
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(search_editText.getWindowToken(), 0);
    }

    public void getCartTotalItem() {
        SessionManager sessionManager = new SessionManager(SearchActivity.this);
        final String userId = sessionManager.getUserid();
        if (!userId.isEmpty()) {
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String totalitem = response.trim();
                    n = Integer.parseInt(totalitem);
                    if (n != 0) {
                        txtViewCount.setVisibility(View.VISIBLE);
                        txtViewCount.setText(totalitem);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if( error instanceof NetworkError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof AuthFailureError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof ServerError) {
                        errormsg="The server could not be found. Please try again after some time!!";
                    }
                    else if (error instanceof ParseError) {
                        errormsg="Parsing error! Please try again after some time!!";
                    }
                    else if (error instanceof NoConnectionError) {
                        errormsg = "Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof TimeoutError) {
                        errormsg = "Connection TimeOut! Please check your internet connection.";
                    }
                    Toast.makeText(getApplication(), errormsg, Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", userId);
                    params.put("action", "total_cart_item");
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }
}