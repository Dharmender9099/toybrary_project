package com.toybraryapp.childs.toybrary.model;

/**
 * Created by Dharmender on 04-12-2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
