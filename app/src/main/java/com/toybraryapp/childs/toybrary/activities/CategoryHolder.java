package com.toybraryapp.childs.toybrary.activities;

/**
 * Created by Dharmender on 01-11-2017.
 */

public class CategoryHolder {
    private String PARENTCATEGORYID;
    private int FLAG_INDICATOR;
    String parentid;

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    Boolean isChecked;

    public CategoryHolder(String PARENTCATEGORYID,int FLAG_INDICATOR,String parentid)
    {
        this.PARENTCATEGORYID=PARENTCATEGORYID;
        this.FLAG_INDICATOR=FLAG_INDICATOR;
       this.parentid=parentid;
    }
    public String getPARENTCATEGORY() {
        return PARENTCATEGORYID;
    }

    public void setPARENTCATEGORY(String PARENTCATEGORYID) {
        this.PARENTCATEGORYID = PARENTCATEGORYID;
    }

    public int getFLAG_INDICATOR() {
        return FLAG_INDICATOR;
    }

    public void setFLAG_INDICATOR(int FLAG_INDICATOR) {
        this.FLAG_INDICATOR = FLAG_INDICATOR;
    }

    public String getParentId() {
        return parentid;
    }

    public void setParentId(String parentId) {
        this.parentid = parentid;
    }


}
