package com.toybraryapp.childs.toybrary.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.MyorderAdapter;
import com.toybraryapp.childs.toybrary.model.Gallery;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dharmender on 14-01-2018.
 */

public class MyOrder_Fragment extends Fragment {
    RecyclerView myorder_recyclerview;
    CardView myordercardview;
     List<Gallery> list;
    Context context;
    String errormsg;
    String userid;
    RecyclerView.LayoutManager layoutManager;
    String action="myorder";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.myorder_fragment,container,false);
        myorder_recyclerview=(RecyclerView)view.findViewById(R.id.myorder_recyclerview);
        myordercardview=(CardView)view.findViewById(R.id.order_cardview);
        SessionManager sessionManager=new SessionManager(getActivity());
        userid=sessionManager.getUserid();
        layoutManager=new LinearLayoutManager(context);
        myOrder();
        return view;
    }
    public void myOrder()
    {
        StringRequest myorder_request=new StringRequest(StringRequest.Method.POST, URLs.MYACCOUNT_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                list=new ArrayList<>();
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("myorder");
                    for(int i=0;i<jsonArray.length();i++)
                    {
                        Gallery gallery=new Gallery();
                        JSONObject obj= (JSONObject) jsonArray.get(i);
                        gallery.setDeliverdate(obj.getString("deliverdate"));
                        gallery.setId(obj.getString("orderid"));
                        gallery.setName(obj.getString("name"));
                        gallery.setImage(URLs.SEARCH_IMAGE_PATH+obj.getString("productImage"));
                        gallery.setProductid(obj.getString("productid"));
                        list.add(gallery);
                    }
                    MyorderAdapter myorderAdapter=new MyorderAdapter(getActivity(),list);
                    myorder_recyclerview.setItemAnimator(new DefaultItemAnimator());
                    myorder_recyclerview.setLayoutManager(layoutManager);
                    myorder_recyclerview.setAdapter(myorderAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";

                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(context,errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("action",action);
                map.put("userid",userid);
                return map;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(myorder_request);
    }
}
