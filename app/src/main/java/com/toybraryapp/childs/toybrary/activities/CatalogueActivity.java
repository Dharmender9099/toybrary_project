
package com.toybraryapp.childs.toybrary.activities;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.CatalogueAdapter;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.OnLoadMoreListener;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.Counter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CatalogueActivity extends AppCompatActivity {
    List<Favourites> cataloguelist;
    RecyclerView catalogueRecyclerView;
    GridLayoutManager manager;
    TextView txtViewCount;
    String errormsg="";
    int n;
    Counter counter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);
        ActionBar actionBar = getSupportActionBar(); // or getActionBar();
        getSupportActionBar().setTitle("Catalogue");
        catalogueRecyclerView=(RecyclerView)findViewById(R.id.cataloguerecyclerView);
        manager = new GridLayoutManager(this, 2);
        catalogueRecyclerView.setLayoutManager(manager);
        SessionManager sessionManager=new SessionManager(this);
        Boolean isLogin=sessionManager.isLogin();
        catalogueImage();
        getCartTotalItem();
        counter=new Counter() {
            @Override
            public void setCounter(int count) {
                n=n+count;
                invalidateOptionsMenu();
            }
        };
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.addcart);
        MenuItemCompat.setActionView(menuItem, R.layout.cart_icon_toolbar);
        RelativeLayout mycarttoolbar = (RelativeLayout) MenuItemCompat.getActionView(menuItem);
        RelativeLayout relativeLayout=(RelativeLayout)mycarttoolbar.findViewById(R.id.relative_layout_item_count);
        txtViewCount = (TextView) mycarttoolbar.findViewById(R.id.badge_notification_1);
       if(n!=0) {
            txtViewCount.setVisibility(View.VISIBLE);
            txtViewCount.setText(String.valueOf(n));
        }
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CatalogueActivity.this, ActivityMy_Cart.class));
            }
        });
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.contactus) {
            startActivity(new Intent(CatalogueActivity.this, ContactUsActivity.class));
        } else if (id == R.id.logout) {
            SharedPreferences preferences =getSharedPreferences("myPref1",MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
            finish();
        }
        else if(id==R.id.editprofile)
        {
            startActivity(new Intent(this,EditProfileActivity.class));
        }
        else if(id==R.id.changepass)
        {
            startActivity(new Intent(this,ChangePasswordActivity.class));
        }
        else if(id==R.id.privacy)
        {
            startActivity(new Intent(this,PolicyActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }
    public void catalogueImage() {
        JsonArrayRequest searchReq = new JsonArrayRequest(URLs.CATALOGUE_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(final JSONArray response) {
                        cataloguelist=new ArrayList<>();
                        for (int i = 0; i<20; i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Favourites favourites=new Favourites();
                                favourites.setId(obj.getString("productId"));
                                favourites.setImage(URLs.SEARCH_IMAGE_PATH+obj.getString("productPhoto"));
                                favourites.setName(obj.getString("productName"));
                                favourites.setNumber(obj.getString("productPoints"));
                                favourites.setStock(obj.getString("productStatus"));
                                cataloguelist.add(favourites);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        // notifying list adapter about data changes
                        final CatalogueAdapter catalogueAdapter = new CatalogueAdapter(catalogueRecyclerView,CatalogueActivity.this,cataloguelist,counter);
                        catalogueRecyclerView.setItemAnimator(new DefaultItemAnimator());
                        catalogueRecyclerView.setAdapter(catalogueAdapter);
                        catalogueAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                cataloguelist.add(null);
                                catalogueAdapter.notifyItemInserted(cataloguelist.size()-1);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        cataloguelist.remove(cataloguelist.size()-1);
                                        catalogueAdapter.notifyItemRemoved(cataloguelist.size());
                                        int index=cataloguelist.size();
                                        int end=index+10;
                                        for(int i=index;i<end;i++) {
                                            try {

                                                JSONObject obj = response.getJSONObject(i);

                                                Favourites favourites = new Favourites();
                                                favourites.setId(obj.getString("productId"));
                                                favourites.setImage(URLs.SEARCH_IMAGE_PATH + obj.getString("productPhoto"));
                                                favourites.setName(obj.getString("productName"));
                                                favourites.setNumber(obj.getString("productPoints"));
                                                favourites.setStock(obj.getString("productStatus"));
                                                cataloguelist.add(favourites);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                        catalogueAdapter.setLoaded();
                                    }
                                },4000);
                            }
                        });
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(searchReq);
    }
    public void getCartTotalItem()
    {
        SessionManager sessionManager=new SessionManager(CatalogueActivity.this);
        final String userId=sessionManager.getUserid();
        if(!userId.isEmpty()) {
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    String res = response.trim();
                    String totalitem = res;
                    n = Integer.parseInt(totalitem);
                    if(n!=0) {
                        txtViewCount.setVisibility(View.VISIBLE);
                        txtViewCount.setText(totalitem);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if( error instanceof NetworkError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof AuthFailureError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof ServerError) {
                        errormsg="The server could not be found. Please try again after some time!!";
                    }
                    else if (error instanceof ParseError) {
                        errormsg="Parsing error! Please try again after some time!!";
                    }
                    else if (error instanceof NoConnectionError) {
                        errormsg = "Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof TimeoutError) {
                        errormsg = "Connection TimeOut! Please check your internet connection.";
                    }
                    Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", userId);
                    params.put("action", "total_cart_item");
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        getCartTotalItem();
        invalidateOptionsMenu();
    }
}
