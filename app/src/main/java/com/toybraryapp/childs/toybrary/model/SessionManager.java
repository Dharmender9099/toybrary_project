package com.toybraryapp.childs.toybrary.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dharmender on 23-11-2017.
 */

public class SessionManager{

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
     Context context;

    int PRIVATE_MODE=0;
    String PREF_NAME="myPref1";

    public String KEY_NAME="username";

    public String KEY_USERID="userid";

    public static final String IS_LOGIN = "IsLoggedIn";

    public SessionManager(Context context)
    {
        this.context=context;
     sharedPreferences=context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
     editor=sharedPreferences.edit();
    }

    public void  createLoginSession(String name, String userid){

        editor.putString(KEY_NAME, name);

        // Storing email in pref
        editor.putString(KEY_USERID, userid);

        editor.putBoolean(IS_LOGIN, true);

        // commit changes
        editor.commit();
    }
    public String getUserid() {
        sharedPreferences = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        return sharedPreferences.getString(KEY_USERID, "");
    }

    public String getUsername()
    {
       SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        return sharedPreferences.getString(KEY_NAME, "");
    }

    public boolean isLogin()
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        return sharedPreferences.getBoolean(IS_LOGIN,false);
    }

}
