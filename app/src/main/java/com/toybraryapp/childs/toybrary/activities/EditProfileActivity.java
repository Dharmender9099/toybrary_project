package com.toybraryapp.childs.toybrary.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity {
EditText firstname,lastname,email,mobile,address,country,city,postalcode;
String userid,errormsg;
LinearLayout statelayout;
TextView state;
List<String> statelist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ActionBar actionBar = getSupportActionBar(); // or getActionBar();
        getSupportActionBar().setTitle("Edit Profile"); // set the top title
        SessionManager sessionManager=new SessionManager(this);
        userid=sessionManager.getUserid();
        init();

       statelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             stateList();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_accept, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.action_settings)
        {
         editProfile();
        }
        return super.onOptionsItemSelected(item);
    }
    public void init()
    {
        firstname=(EditText)findViewById(R.id.firstname);
        lastname=(EditText)findViewById(R.id.lastname);
        email=(EditText)findViewById(R.id.email);
        mobile=(EditText)findViewById(R.id.mobile);
        address=(EditText)findViewById(R.id.address);
        country=(EditText)findViewById(R.id.country);
        city=(EditText)findViewById(R.id.city);
        postalcode=(EditText)findViewById(R.id.postalcode);
        state=(TextView)findViewById(R.id.state);
        statelayout=(LinearLayout)findViewById(R.id.state_layout);
        statelist=new ArrayList<>();
        getRecord();

    }
    public void getRecord()
    {
        StringRequest stringRequest=new StringRequest(StringRequest.Method.POST, URLs.PROFILE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("getrecord");
                    for(int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        firstname.setText(jsonObject1.getString("firstname"));
                        lastname.setText(jsonObject1.getString("lastname"));
                        email.setText(jsonObject1.getString("email"));
                        mobile.setText(jsonObject1.getString("mobile"));
                        address.setText(jsonObject1.getString("shippingaddress"));
                        country.setText(jsonObject1.getString("shippingcountry"));
                        city.setText(jsonObject1.getString("shippingcity"));
                        state.setText(jsonObject1.getString("shippingstate"));
                        postalcode.setText(jsonObject1.getString("shippingpostalcode"));

                    }

                    JSONArray jsonArray1=jsonObject.getJSONArray("state");
                    for(int i=0;i<jsonArray1.length();i++)
                    {
                        JSONObject jsonObject1= (JSONObject) jsonArray1.get(i);
                        statelist.add(jsonObject1.getString("state"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg= "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("userid",userid);
                map.put("action","getrecord");
                return map;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
    public void stateList()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Select State").setAdapter(new ArrayAdapter(this,android.R.layout.simple_list_item_1,statelist), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                state.setText(statelist.get(i));
            }
        });

                builder.show();
    }

    public void editProfile()
    {
        final String user_firstname=firstname.getText().toString();
        final String user_lastname=lastname.getText().toString();
        final String user_email=email.getText().toString();
        final String user_mobile=mobile.getText().toString();
        final String user_address=address.getText().toString();
        final String user_country=country.getText().toString();
        final String user_city=city.getText().toString();
        final String user_state=city.getText().toString();
        final String user_postalcode=postalcode.getText().toString();
        StringRequest editReq=new StringRequest(StringRequest.Method.POST, URLs.PROFILE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            String res=response.trim();
            if(res.equals("Your profile updated succesfully"))
            {
                Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
                startActivity(new Intent(EditProfileActivity.this,MainActivity.class));
            }
            else
            {
                Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg= "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("action","editprofile");
                map.put("userid",userid);
                map.put("firstname",user_firstname);
                map.put("lastname",user_lastname);
                map.put("email",user_email);
                map.put("address",user_address);
                map.put("city",user_city);
                map.put("state",user_state);
                map.put("mobile",user_mobile);
                map.put("country",user_country);
                map.put("postalcode",user_postalcode);
                return map;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(editReq);
    }
}
