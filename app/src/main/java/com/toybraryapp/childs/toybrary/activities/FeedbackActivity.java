package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends AppCompatActivity {
    LinearLayout linearLayout;
EditText name,email,rating,mobile,feedback;
Button send,ratingbutton;
    ProgressDialog progressDialog;
    String errormsg="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        linearLayout=(LinearLayout)findViewById(R.id.linearLayout);
        setContentView(R.layout.activity_feedback);
        name=(EditText)findViewById(R.id.name);
        email=(EditText)findViewById(R.id.email);
        rating=(EditText)findViewById(R.id.ratingtext);
        ratingbutton=(Button)findViewById(R.id.ratingbutton);
        mobile=(EditText)findViewById(R.id.mobile);
        feedback=(EditText)findViewById(R.id.feedback);
        send=(Button)findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userFeedback();
            }
        });
        ratingbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rating=(EditText)findViewById(R.id.ratingtext);
                final String[] mrating={"1","2","3","4","5"};
                android.app.AlertDialog.Builder bb=new android.app.AlertDialog.Builder(FeedbackActivity.this);
                bb.setTitle("Select Rating");
                bb.setSingleChoiceItems(mrating, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        rating.setText(mrating[i].toString());
                        dialogInterface.dismiss();
                    }
                });
                bb.show();
            }
        });

    }
    private void userFeedback() {

        final String user_name=name.getText().toString();
        final String user_email=email.getText().toString();
        final String user_rating=rating.getText().toString();
        final String user_mobile=mobile.getText().toString();
        final String user_feedback=feedback.getText().toString();

        if(TextUtils.isEmpty(user_name))
        {
            name.setError("please enter name");
            name.requestFocus();
            return;
        }
        progressDialog = ProgressDialog.show(this, "", "Please wait...");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.FEEDBACK_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                progressDialog.dismiss();
                if(res.equals("thanks for feedback"))
                {
                    //Snackbar snackbar=Snackbar.make(linearLayout,"Thanks for your valueable feedback",Snackbar.LENGTH_LONG);
                    //snackbar.setActionTextColor(Color.YELLOW);
                   // snackbar.show();
                    startActivity(new Intent(FeedbackActivity.this,MainActivity.class));
                    Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();


                }
                else if(res.equals("Mail sent successfully"))
                {
                    //Snackbar snackbar=Snackbar.make(linearLayout,"something went wrong",Snackbar.LENGTH_LONG);
                    //snackbar.setActionTextColor(Color.RED);
                    //snackbar.show();
                    Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
                }
                else if(res.equals("Message could not sent"))
                {
                    Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(getApplication(),response,Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
              Map<String,String> params=new HashMap<>();
              params.put("name",user_name);
              params.put("email",user_email);
              params.put("rating",user_rating);
              params.put("mobile",user_mobile);
              params.put("feedback",user_feedback);
              return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}
