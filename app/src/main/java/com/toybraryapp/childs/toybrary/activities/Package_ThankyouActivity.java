package com.toybraryapp.childs.toybrary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.Gallery;

public class Package_ThankyouActivity extends AppCompatActivity {
    TextView txt_continue,points;
    public static String pro_points;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package__thankyou);
        Gallery gallery=new Gallery();
        txt_continue=(TextView)findViewById(R.id.txt_continue);
        points=(TextView)findViewById(R.id.points);
        points.setText(pro_points);
        txt_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Package_ThankyouActivity.this,MainActivity.class));
            }
        });
    }
}
