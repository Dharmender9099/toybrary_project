package com.toybraryapp.childs.toybrary.volleyRequest;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dharmender on 21-02-2018.
 */

public class JsonParserVolley {
    Context context;
    RequestQueue requestQueue;
    String jsonresponse;

    private Map<String, String> header;

    public JsonParserVolley(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
        header = new HashMap<>();

    }

    public void addHeader(String key, String value) {
        header.put(key, value);
    }

    public void executeRequest(int method,String JsonURL,final VolleyCallback callback) {

        StringRequest stringRequest = new StringRequest(method, JsonURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                jsonresponse = response;
                Log.e("RES", " res::" + jsonresponse);
                callback.getResponse(jsonresponse);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.getError(error);
                Toast.makeText(context,String.valueOf(error),Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return header;
            }
        };

        requestQueue.add(stringRequest);

    }
    public interface VolleyCallback
    {
        public void getResponse(String response);
        public void getError(VolleyError error);
    }

}
