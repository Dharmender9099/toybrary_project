package com.toybraryapp.childs.toybrary.adapter;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.activities.SingleProductActivity;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.OnLoadMoreListener;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.Counter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dharmender on 12-12-2017.
 */

public class CatalogueAdapter extends RecyclerView.Adapter{
    private List<Favourites> list=new ArrayList<>();
    private boolean isLoading;
    private Activity activity;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    Counter counter;
    public CatalogueAdapter(RecyclerView recyclerView,Activity activity, List<Favourites> list,Counter counter)
    {
        this.activity=activity;
        this.list=list;
        this.counter=counter;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }
    }
    @Override
    public int getItemViewType(int position) {
        return list.get(position)== null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        RecyclerView.ViewHolder vh = null;
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.image_layout, parent, false);
            vh= new FavouritesViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.layout_loading_item, parent, false);
         vh=new LoadingViewHolder(view);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FavouritesViewHolder) {
            Favourites favourites = list.get(position);
            final FavouritesViewHolder favouritesViewHolder = (FavouritesViewHolder) holder;
            Glide.with(activity).load(favourites.getImage()).into(favouritesViewHolder.imageView);
            favouritesViewHolder.name.setText(favourites.getName());
            favouritesViewHolder.itemId.setText(favourites.getId());
            favouritesViewHolder.number.setText(favourites.getNumber());
            favouritesViewHolder.stock.setText(favourites.getStock());
            favouritesViewHolder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(activity, SingleProductActivity.class);
                    String value=favouritesViewHolder.itemId.getText().toString();
                    intent.putExtra("productId",value);
                    activity.startActivity(intent);
                }
            });
            favouritesViewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(activity, SingleProductActivity.class);
                    String value=favouritesViewHolder.itemId.getText().toString();
                    intent.putExtra("productId",value);
                    activity.startActivity(intent);
                }
            });
            favouritesViewHolder.addtocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SessionManager sessionManager=new SessionManager(activity);
                    String userId=sessionManager.getUserid();
                    String checkquantity=favouritesViewHolder.stock.getText().toString();
                    if(checkquantity.equals("out of stock!"))
                    {
                    Toast.makeText(activity,"Product out of stock!",Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (!userId.isEmpty()) {
                            String productId = favouritesViewHolder.itemId.getText().toString();
                            String add = "additem";
                            updateCart(productId, userId, add);
                        } else {
                            Toast.makeText(activity, "please register first!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
        }
        else if(holder instanceof LoadingViewHolder)
        {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }
    public void updateCart(final String productid, final String userid,final String additem)
    {
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                Toast.makeText(activity,res,Toast.LENGTH_LONG).show();
                if(res.equals("product successfully added in cart")) {
                    counter.setCounter(1);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity,String.valueOf(error),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("product_id",productid);
                params.put("user_id",userid);
                params.put("action",additem);
                return params;
            }
        };
        VolleySingleton.getInstance(activity).addToRequestQueue(stringRequest);
    }
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }
    @Override
    public int getItemCount(){
        return list.size();
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }

    class FavouritesViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,addtocart;
        TextView name,number,itemId,stock;

        public FavouritesViewHolder(View itemView) {
            super(itemView);
            itemId=(TextView)itemView.findViewById(R.id.itemId);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            name = (TextView) itemView.findViewById(R.id.name);
            number = (TextView) itemView.findViewById(R.id.number);
            addtocart=(ImageView)itemView.findViewById(R.id.addtocart);
            stock=(TextView)itemView.findViewById(R.id.stock);
        }
    }

    public void setLoaded() {
        isLoading = false;
    }

}
