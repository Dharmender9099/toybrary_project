package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

public class SuggestToyActivity extends AppCompatActivity {

EditText name,mobile,suggesttoy;
Button send;
    ProgressDialog progressDialog;
    String errormsg="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_toy);
        name=(EditText)findViewById(R.id.name);
        mobile=(EditText)findViewById(R.id.mobile);
        suggesttoy=(EditText)findViewById(R.id.suggesttoy);
        send=(Button)findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suggestToy();
            }
        });
    }
    private void suggestToy()
    {
        final String user_name=name.getText().toString();
        final String user_mobile=mobile.getText().toString();
        final String user_suggesttoy=suggesttoy.getText().toString();

        if(user_name.isEmpty())
        {
            name.setError("please enter name");
            name.requestFocus();
            return;
        }
        if(user_mobile.isEmpty())
        {
            mobile.setError("please enter mobileno");
            mobile.requestFocus();
            return;
        }
        if(user_mobile.length()>10)
        {
            mobile.setError("please enter any 10 digit no");
            mobile.requestFocus();
            return;
        }
        if(user_suggesttoy.isEmpty())
        {
            suggesttoy.setError("please enter any query");
            suggesttoy.requestFocus();
            return;
        }
        progressDialog = ProgressDialog.show(this, "", "Please wait...");
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URLs.SUGGESTTOY_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                progressDialog.dismiss();
          if(res.equals("your query has been registered"))
          {
              startActivity(new Intent(SuggestToyActivity.this,MainActivity.class));
              Toast.makeText(getApplicationContext(),"your request sucessfully registered",Toast.LENGTH_LONG).show();
          }
          else
          {
              Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
          }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("name",user_name);
                map.put("mobile",user_mobile);
                map.put("suggesttoy",user_suggesttoy);
                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 80000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }
}
