package com.toybraryapp.childs.toybrary.activities;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Calendar;

import android.widget.DatePicker;
import android.app.DatePickerDialog;

import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;

public class ChildDetialsActivity extends AppCompatActivity implements View.OnClickListener {
   EditText childname,year;
   TextView datepick,dateerror;
   Button next;
    DatePickerDialog datePickerDialog;
    String userId;
    String[] alldetails,userdetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_detials);
        childname=(EditText)findViewById(R.id.childname);
        year=(EditText)findViewById(R.id.year);
        datepick=(TextView)findViewById(R.id.datepick);
        next=(Button)findViewById(R.id.next);
        dateerror=(TextView)findViewById(R.id.dateerror);

        datepick.setOnClickListener(this);
        alldetails=getIntent().getStringArrayExtra("userAddress");
        SessionManager sessionManager=new SessionManager(this);
        userId=sessionManager.getUserid();
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                childDetail();
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.datepick:
             ondatePick();
             break;
        }
    }

    public void childDetail()
    {

        final String mchildname=childname.getText().toString();
        final String myear=year.getText().toString();
        if(TextUtils.isEmpty(mchildname))
        {
            childname.setError("please enter child name");
            childname.requestFocus();
            return;
        }
        if(myear.equals(""))
        {
            dateerror.setVisibility(View.VISIBLE);
            dateerror.setText("please enter child date of birth");
        }
        else
        {
            dateerror.setVisibility(View.GONE);
            dateerror.setText("");
        }
         userdetails=new String[]{alldetails[0],alldetails[1],alldetails[2],alldetails[3],alldetails[4],alldetails[5],alldetails[6],alldetails[7],
         alldetails[8],alldetails[9],alldetails[10],alldetails[11],alldetails[12],mchildname,myear};
        Intent intent=new Intent(ChildDetialsActivity.this,PaymentActivity1.class);
        intent.putExtra("user_details",userdetails);
        startActivity(intent);
        /*     progressDialog = ProgressDialog.show(this, "", "Please wait...");
       StringRequest stringRequest=new StringRequest(Request.Method.POST, URLs.SHIPING_URL, new Response.Listener<String>() {
           @Override
           public void onResponse(String response) {
               progressDialog.dismiss();
           String res=response.trim();
           if(res.equals("your package request successfully registered"))
           {
               Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
               startActivity(new Intent(ChildDetialsActivity.this,MainActivity.class));
           }
           else {
               Toast.makeText(getApplication(),response,Toast.LENGTH_LONG).show();
           }
           }
       }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
               progressDialog.dismiss();
               Toast.makeText(getApplication(),String.valueOf(error),Toast.LENGTH_LONG).show();
           }
       })
       {
           @Override
           protected Map<String, String> getParams() throws AuthFailureError {
               Map<String,String> params=new HashMap();
               params.put("userid",userId);
               params.put("packageid",alldetails[0]);
               params.put("promocode",alldetails[1]);
               params.put("depositamount",alldetails[2]);
               params.put("billingaddress",alldetails[3]);
               params.put("billingcountry",alldetails[4]);
               params.put("billingstate",alldetails[5]);
               params.put("billingcity",alldetails[6]);
               params.put("billingpostalcode",alldetails[7]);
               params.put("shipingaddress",alldetails[8]);
               params.put("shipingcity",alldetails[9]);
               params.put("shipingpostalcode",alldetails[10]);
               params.put("shipingstate",alldetails[11]);
               params.put("shipingcountry",alldetails[12]);
               params.put("childname",mchildname);
               params.put("childbirthdate",myear);
               return params;
           }
       };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);*/
    }
    public void ondatePick()
    {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(ChildDetialsActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int myear,
                                          int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        year.setText(dayOfMonth + "/"
                                + (monthOfYear + 1) + "/" + myear);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}
