package com.toybraryapp.childs.toybrary.activities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.GalleryAdapter;
import com.toybraryapp.childs.toybrary.adapter.VideoAdapter0;
import com.toybraryapp.childs.toybrary.fragment.SlideshowDialogFragment;
import com.toybraryapp.childs.toybrary.model.Gallery;
import com.toybraryapp.childs.toybrary.model.Video;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends AppCompatActivity {
    private static final String TAG = GalleryActivity.class.getSimpleName();
    List<Gallery> list;
    GalleryAdapter galleryAdapter;
    RecyclerView recyclerView;
    String errormsg="";
    RecyclerView mVideosRecyclerView;
    private List<Video> mVideosList;
    private VideoAdapter0 mVideoAdapter;
    GridLayoutManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        mVideosRecyclerView = (RecyclerView) findViewById(R.id.videoRecyclerView);
        manager = new GridLayoutManager(this, 2);
        mVideosRecyclerView.setLayoutManager(manager);
        mVideosList = new ArrayList<>();
        mVideoAdapter=new VideoAdapter0(this,mVideosList);
        mVideosRecyclerView.setAdapter(mVideoAdapter);

        recyclerView=(RecyclerView)findViewById(R.id.imagerecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        list=new ArrayList<>();
        galleryAdapter=new GalleryAdapter(GalleryActivity.this,list);
        recyclerView.setAdapter(galleryAdapter);
        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", (Serializable) list);
                bundle.putInt("position", position);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        galleryImage();
        galleryVideo();
    }

    public void galleryImage()
    {
        JsonArrayRequest galleryReq = new JsonArrayRequest(URLs.Gallery_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Gallery gallery = new Gallery();
                                gallery.setImage(URLs.GALLERY_IMAGE_URL+obj.getString("image"));

                                gallery.setName(obj.getString("name"));
                                list.add(gallery);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    galleryAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(galleryReq);
    }

    public void galleryVideo()
    {
        JsonArrayRequest videoReq = new JsonArrayRequest(URLs.VIDEO_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Video video = new Video();
                                video.setVideoImage(obj.getString("image"));
                                video.setVideoTitle(obj.getString("title"));
                                mVideosList.add(video);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                  mVideoAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(videoReq);
    }
}

