package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.activities.SingleProductActivity;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.Counter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dharmender on 03-01-2018.
 */

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.GridViewHolder> {
    private List<Favourites> list=new ArrayList<>();
    private Context context;
    Counter counter;
    Drawable mDrawable;
    int quant;
    public GridAdapter(Context context,List<Favourites> list,Counter counter)
    {
        this.context=context;
        this.list=list;
        this.counter=counter;
    }
    @Override
    public GridAdapter.GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.image_layout,null);
        return new GridAdapter.GridViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GridAdapter.GridViewHolder holder, final int position){
        Favourites favourites=list.get(position);
        Glide.with(context)
                .load(favourites.getImage())
                .into(holder.imageView);
        holder.name.setText(favourites.getName());
        holder.itemId.setText(favourites.getId());
        holder.number.setText(favourites.getNumber());
        holder.stock.setText(favourites.getStock());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, SingleProductActivity.class);
                String value=holder.itemId.getText().toString();
                intent.putExtra("productId",value);
                context.startActivity(intent);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, SingleProductActivity.class);
                String value=holder.itemId.getText().toString();
                intent.putExtra("productId",value);
                context.startActivity(intent);
            }
        });
        holder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String checkquantity=holder.stock.getText().toString();
                SessionManager sessionManager=new SessionManager(context);
                String userId=sessionManager.getUserid();
                if(checkquantity.equals("out of stock!")) {
                    Toast.makeText(context,"Product out of stock!",Toast.LENGTH_LONG).show();
                }
                else {
                    if (!userId.isEmpty()) {
                        String productId = holder.itemId.getText().toString();
                        String add = "additem";
                        updateCart(productId, userId, add);
                    } else {
                        Toast.makeText(context, "please register first!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
    public void updateCart(final String productid, final String userid,final String additem)
    {
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                Toast.makeText(context,res,Toast.LENGTH_LONG).show();
                if(res.equals("product successfully added in cart")) {
                    counter.setCounter(1);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,String.valueOf(error),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("product_id",productid);
                params.put("user_id",userid);
                params.put("action",additem);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    class GridViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,addtocart;
        TextView name, number,itemId,stock;

        public GridViewHolder(View itemView) {
            super(itemView);
            itemId=(TextView)itemView.findViewById(R.id.itemId);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            name = (TextView) itemView.findViewById(R.id.name);
            number = (TextView) itemView.findViewById(R.id.number);
            addtocart=(ImageView)itemView.findViewById(R.id.addtocart);
            stock=(TextView)itemView.findViewById(R.id.stock);
        }
    }
}
