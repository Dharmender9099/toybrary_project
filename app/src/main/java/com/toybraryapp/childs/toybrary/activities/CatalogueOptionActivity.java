package com.toybraryapp.childs.toybrary.activities;

import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.CategoryAdapter;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.OnLoadMoreListener;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CatalogueOptionActivity extends AppCompatActivity {
    ArrayList<Favourites> agelist;
    ArrayList<String> points;
    List<Favourites> pointslist;
    List<Favourites> brandlist;
    RecyclerView recyclerView;
    String errormsg="";
    RecyclerView.LayoutManager manager;
    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue_option);
        ActionBar actionBar = getSupportActionBar(); // or getActionBar();
        String value = getIntent().getStringExtra("value");
       recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
       manager=new LinearLayoutManager(this);
        points = new ArrayList<>();
        points.add("1-5");
        points.add("6-10");
        points.add("11-15");
        points.add("16-20");

        if (value.equals("brands")) {
            getSupportActionBar().setTitle("Brands");
            brandItem();
        }
        else if(value.equals("age"))
        {
            getSupportActionBar().setTitle("Age");
            ageItem();

        }
        else if(value.equals("points"))
        {
            getSupportActionBar().setTitle("Points");
            pointslist=new ArrayList<>();
            for(int i=0;i<points.size();i++)
            {
              Favourites favourites=new Favourites();
              favourites.setName(points.get(i));
              pointslist.add(favourites);
            }
            CategoryAdapter categoryAdapter=new CategoryAdapter(recyclerView,CatalogueOptionActivity.this,pointslist);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setLayoutManager(manager);
            recyclerView.setAdapter(categoryAdapter);
        }

    }

    public void brandItem()
    {
        JsonArrayRequest brandReq = new JsonArrayRequest(URLs.BRANDS_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(final JSONArray response) {
                        brandlist = new ArrayList<>();
                        for (int i = 0; i < response.length(); i++) {

                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Favourites favourites=new Favourites();
                                favourites.setId(obj.getString("brandId"));
                                favourites.setIdentity("brands");
                                favourites.setName(obj.getString("brandName"));
                                brandlist.add(favourites);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        final CategoryAdapter categoryAdapter=new CategoryAdapter(recyclerView,CatalogueOptionActivity.this,brandlist);
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(categoryAdapter);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        categoryAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                if(brandlist.size()<=20)
                                {
                                 brandlist.add(null);
                                 categoryAdapter.notifyItemInserted(brandlist.size()-1);
                                 new Handler().postDelayed(new Runnable() {
                                     @Override
                                     public void run() {
                                  brandlist.remove(brandlist.size()-1);
                                  categoryAdapter.notifyItemRemoved(brandlist.size());

                                         int index = brandlist.size();
                                         int end = index + 10;
                                         for (int i = index; i < end; i++) {
                                             JSONObject obj = null;
                                             try {
                                                 obj = response.getJSONObject(i);
                                                 Favourites favourites=new Favourites();
                                                 favourites.setId(obj.getString("brandId"));
                                                 favourites.setIdentity("brands");
                                                 favourites.setName(obj.getString("brandName"));
                                                 brandlist.add(favourites);
                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }

                                         }
                                         categoryAdapter.notifyDataSetChanged();
                                         categoryAdapter.setLoaded();
                                     }
                                 },5000);
                                }
                            }
                        });
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();

            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(brandReq);
    }

    public void ageItem()
    {
        JsonArrayRequest ageReq = new JsonArrayRequest(URLs.AGE_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        agelist= new ArrayList<>();
                        for (int i = 0; i < response.length(); i++) {

                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Favourites favourites=new Favourites();
                                favourites.setId(obj.getString("ageId"));
                                favourites.setIdentity("age");
                                favourites.setName( obj.getString("ageName"));
                                agelist.add(favourites);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        CategoryAdapter categoryAdapter=new CategoryAdapter(recyclerView,CatalogueOptionActivity.this,agelist);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(categoryAdapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();

            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(ageReq);
    }

}
