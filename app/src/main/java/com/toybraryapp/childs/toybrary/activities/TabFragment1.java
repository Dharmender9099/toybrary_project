package com.toybraryapp.childs.toybrary.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.toybraryapp.childs.toybrary.R;

/**
 * Created by Dharmender on 14-01-2018.
 */

public class TabFragment1 extends Fragment {
    static int count=2;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tabfragment1_layout,container,false);
        TabLayout tabLayout=(TabLayout)view.findViewById(R.id.tablayout);
        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setAdapter(new Myadapter(getChildFragmentManager()));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {

            }
        });
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }
    public class Myadapter extends FragmentPagerAdapter
    {

        public Myadapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position)
            {
                case 0:
                    return new MyOrder_Fragment();
                case 1:
                    return new MyPackage_Fragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position)
            {
                case 0:
                    return "MY ORDER";
                case 1:
                    return "MY PACKAGE";
            }
            return null;
        }
    }
}
