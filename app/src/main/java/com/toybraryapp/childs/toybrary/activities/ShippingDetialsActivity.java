package com.toybraryapp.childs.toybrary.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.toybraryapp.childs.toybrary.R;

public class ShippingDetialsActivity extends AppCompatActivity implements View.OnClickListener{
EditText address,city,postalcode;
TextView country,state;
LinearLayout country_layout,state_layout;
TextView stateerror,countryerror;
String[] shippingArray,fulldetail;
Button next;
    private String stateArray[]={"West Bengal","Uttar Pradesh","Tripura","Tamil Nadu","Sikkim","Rajasthan","Punjab","Pondicherry","Orissa"
    ,"Nagaland","Mizoram","Meghalaya","Manipur","Maharashtra","Madhya Pradesh","Lakshadweep islands","Kerala","Karnataka","Jammu and Kashmir",
    "Himachal Pradesh","Haryana","Gujarat","Goa","Delhi","Daman and Diu","Dadra and Nagar Haveli","Chandigarh","Bihar","Assam","Arunchal Pradesh",
    "Andhra Pradesh","Andaman and Nicobar islands"};
    private String countryArray[]={"India"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_detials);
        address=(EditText)findViewById(R.id.address);
        city=(EditText)findViewById(R.id.city);
        postalcode=(EditText)findViewById(R.id.postalcode);
        country=(TextView)findViewById(R.id.country);
        country_layout=(LinearLayout)findViewById(R.id.country_layout);
        state=(TextView)findViewById(R.id.state);
        state_layout=(LinearLayout)findViewById(R.id.state_layout);
        stateerror=(TextView)findViewById(R.id.stateerror);
        countryerror=(TextView)findViewById(R.id.countryerror);
        next=(Button)findViewById(R.id.next);
        state_layout.setOnClickListener(this);
        country_layout.setOnClickListener(this);
        next.setOnClickListener(this);
        shippingArray=getIntent().getStringArrayExtra("billingData");
    }

    @Override
    public void onClick(View view) {
      switch(view.getId()){
          case R.id.state_layout:
              onClickState();
              break;
          case R.id.country_layout:
              onClickCountry();
              break;
          case R.id.next:
            shippingdetail();
              break;
      }
    }

    public void shippingdetail()
    {
        String maddress= address.getText().toString();
        String mcity=city.getText().toString();
        String mpostalcode=postalcode.getText().toString();
        String mstate=state.getText().toString();
        String mcountry=country.getText().toString();


        if(TextUtils.isEmpty(maddress))
        {
            address.setError("please enter full address");
            address.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(mcity))
        {
            city.setError("please entery city name");
            city.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(mpostalcode))
        {
            postalcode.setError("please enter postal code");
            postalcode.requestFocus();
            return;
        }
        if(mstate.equals("Select States"))
        {
            stateerror.setVisibility(View.VISIBLE);
            stateerror.setText("please selelct any state");
        }
        else
        {
            stateerror.setVisibility(View.GONE);
            stateerror.setText("");
        }
        if(mcountry.equals("Select Country"))
        {
            countryerror.setVisibility(View.VISIBLE);
            countryerror.setText("please selelct any country");
        }
        else
        {
          countryerror.setVisibility(View.GONE);
            countryerror.setText("");
        }
        fulldetail= new String[]{shippingArray[0],shippingArray[1],shippingArray[2],shippingArray[3],shippingArray[4],shippingArray[5],shippingArray[6],shippingArray[7],maddress
                ,mcity,mpostalcode,mstate,mcountry};
       Intent intent=new Intent(ShippingDetialsActivity.this,ChildDetialsActivity.class);
        intent.putExtra("userAddress",fulldetail);
        startActivity(intent);
    }
    public void onClickState()
    {

        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Please select any state");
        builder.setSingleChoiceItems(stateArray, 2, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                state.setText(stateArray[i].toString());
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
    public void onClickCountry()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Please select any country");
        builder.setSingleChoiceItems(countryArray, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                country.setText(countryArray[i].toString());
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
public void setData()
{
address.setText(shippingArray[0]);
country.setText(shippingArray[1]);
state.setText(shippingArray[2]);
city.setText(shippingArray[3]);
postalcode.setText(shippingArray[4]);
}

}
