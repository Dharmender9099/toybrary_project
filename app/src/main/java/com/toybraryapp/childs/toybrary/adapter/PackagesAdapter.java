package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.toybraryapp.childs.toybrary.activities.MerchantActivity;
import com.toybraryapp.childs.toybrary.activities.Package_ThankyouActivity;
import com.toybraryapp.childs.toybrary.activities.PackagesActivity;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.Gallery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dharmender on 18-11-2017.
 */

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.PackageHolder>{
    Context context;
    int row_index=-1;
    List<Gallery> list=new ArrayList<>();

    public PackagesAdapter(Context context,List<Gallery> list)
    {
        this.context=context;
        this.list=list;
    }
    @Override
    public PackageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.packages_layout,parent,false);
        return new PackageHolder(view);
    }

    @Override
    public void onBindViewHolder(final PackageHolder holder, final int position) {
        final Gallery gallery=list.get(position);
        holder.packageid.setText(gallery.getId());
     holder.packageTitle.setText(gallery.getTitle());
     holder.packageValidity.setText(gallery.getValidity());
     holder.packagePoints.setText(gallery.getPoints());
     holder.packagePrice.setText(gallery.getPrice());

     holder.package_layout.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            String id= holder.packageid.getText().toString();
             Package_ThankyouActivity.pro_points=holder.packagePoints.getText().toString();
             PackagesActivity.packageid=id;
             MerchantActivity.txnAmount=holder.packagePrice.getText().toString();
          /* ProductPackage productPackage=new ProductPackage();
           productPackage.setId(id);*/
             row_index=position;
             notifyDataSetChanged();
         }
     });
        if(row_index==position){
            holder.package_layout.setBackgroundColor(Color.parseColor("#c9c9c9"));
        }
        else
        {
            holder.package_layout.setBackgroundColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PackageHolder extends RecyclerView.ViewHolder
    {
     TextView packageid,packageTitle,packageValidity,packagePoints,packagePrice;
     LinearLayout package_layout;
        public PackageHolder(View itemView) {
            super(itemView);
            packageid=(TextView)itemView.findViewById(R.id.packageid);
            packageTitle=(TextView)itemView.findViewById(R.id.packageTitle);
            packageValidity=(TextView)itemView.findViewById(R.id.packageValidity);
            packagePoints=(TextView)itemView.findViewById(R.id.packagePoints);
            packagePrice=(TextView)itemView.findViewById(R.id.packagePrice);
            package_layout=(LinearLayout)itemView.findViewById(R.id.package_layout);

        }
    }
}
