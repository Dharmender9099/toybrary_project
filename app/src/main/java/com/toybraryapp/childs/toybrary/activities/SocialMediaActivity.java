package com.toybraryapp.childs.toybrary.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.toybraryapp.childs.toybrary.R;

public class SocialMediaActivity extends AppCompatActivity {
    WebView webView;
    String pageLink;
    ProgressBar progressBar;;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        webView = (WebView) findViewById(R.id.webview);
        webView.setWebViewClient(new MyBrowser());
        String action =getIntent().getStringExtra("action");
        getSupportActionBar().setTitle(action);
        if (action.equals("Facebook")) {
             pageLink = "https://www.facebook.com/toybrary.in";

        }
        else if(action.equals("Twitter"))
        {
          pageLink="https://twitter.com/toybrary2015";
        }
        else if(action.equals("Instagram"))
        {
            pageLink="https://www.instagram.com/toybrary_/";
        }
        else if(action.equals("Linkedin"))
        {
            pageLink="https://www.linkedin.com/in/toy-brary-256a3512b/.linkedin";
        }
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(pageLink);
    }

         class MyBrowser extends WebViewClient {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressBar.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }
             @Override
             public void onPageFinished(WebView view, String url) {
                 // TODO Auto-generated method stub
                 super.onPageFinished(view, url);
                 progressBar.setVisibility(View.GONE);
             }

    }
}
