package com.toybraryapp.childs.toybrary.url;

/**
 * Created by Dharmender on 22-11-2017.
 */

public class URLs {
    static String ip="139.59.94.55"+"/api";
    static String ip1="139.59.94.55";
    public static final String REGISTER_URL="http://"+ip+"/toybrary/register.php";
    public static final String LOGIN_URL="http://"+ip+"/toybrary/login.php";
    public static final String CALLBACK_URL="http://"+ip+"/toybrary/callback.php";
    public static final String FEEDBACK_URL="http://"+ip+"/toybrary/feedback.php";
    public static final String CONTACTUS_URL="http://"+ip+"/toybrary/contactus.php";
    public static final String Gallery_URL="http://"+ip+"/toybrary/gallery.php";
    public static final String GALLERY_IMAGE_URL="http://"+ip1+"/upload/gallery/";
    public static final String SUGGESTTOY_URL="http://"+ip+"/toybrary/suggesttoy.php";
    public static final String SEARCH_URL="http://"+ip+"/toybrary/search.php?searchItem=";
    public static final String SEARCH_IMAGE_PATH="http://"+ip1+"/upload/product/200X200/";
    public static final String PACKAGE_URL="http://"+ip+"/toybrary/packages.php";
    public static final String BRANDS_URL="http://"+ip+"/toybrary/brands.php";
    public static final String AGE_URL="http://"+ip+"/toybrary/age.php";
    public static final String FAVOURITE_TOY_URL="http://"+ip+"/toybrary/favouritesImage.php";
    public static final String FAVOURITE_TOY_PATH="http://"+ip1+"/upload/product/200X200/";
    public static final String NEWLY_TOY_URL="http://"+ip+"/toybrary/newlyItem.php";
    public static final String NEWLY_TOY_PATH="http://"+ip1+"/upload/product/200X200/";
    public static final String FORGOT_PASSWORD="http://"+ip+"/toybrary/forgot-password.php";
    public static final String PRODUCT_DETAILS="http://"+ip+"/toybrary/single-product.php?productId=";
    public static final String CATALOGUE_URL="http://"+ip+"/toybrary/catalogue.php";
    public static final String SHIPING_URL="http://"+ip+"/toybrary/shippingdetail.php";
    public static final String SUBCATEGORY_ITEM="http://"+ip+"/toybrary/allcategories.php";
    public static final String UPDATE_CART="http://"+ip+"/toybrary/user_cart.php";
    public static final String VIEW_CART="http://"+ip+"/toybrary/view_cart.php";
    public static final String CATEGORY="http://"+ip+"/toybrary/category.php";
    public static final String SUB_CATEGORY="http://"+ip+"/toybrary/subcategory.php";
    public static final String VIDEO_PATH="http://"+ip1+"/upload/gallery/video/";
    public static final String VIDEO_URL="http://"+ip+"/toybrary/galleryVideo.php";
    public static final String PAYMENT_PROCESS_URL="http://"+ip+"/toybrary/payment_process.php";
    public static final String CHANGEPASS_URL="http://"+ip+"/toybrary/changepassword.php";
    public static final String PROFILE_URL="http://"+ip+"/toybrary/profile.php";
    public static final String MYACCOUNT_URL="http://"+ip+"/toybrary/myaccount.php";
    public static final String ORDERDETAILS_URL="http://"+ip+"/toybrary/orderdetails.php";
    public static final String REVIEW_URL="http://"+ip+"/toybrary/review.php";
    public static final String TRACKUSER_URL="http://"+ip+"/toybrary/user_track.php";
    public static final String HOMECATEGORY_URL="http://"+ip+"/toybrary/homecategory.php";

}
