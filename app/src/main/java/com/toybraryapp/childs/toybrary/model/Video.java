package com.toybraryapp.childs.toybrary.model;

/**
 * Created by Dharmender on 01-01-2018.
 */

public class Video {
   private String videoTitle;
   private String videoImage;

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }
}
