package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.toybraryapp.childs.toybrary.R;

/**
 * Created by Dharmender on 02-11-2017.
 */

public class CustomViewPagerAdapter extends PagerAdapter {
    Context context;
    private int mImages[];
    private LayoutInflater layoutInflater;
    public CustomViewPagerAdapter(Context context,int[] mImages)
    {
        this.context=context;
        this.mImages=mImages;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView= layoutInflater.inflate(R.layout.single_image,container,false);

        ImageView mImageView = (ImageView)itemView.findViewById(R.id.imageView);
        mImageView.setImageResource(mImages[position]);

        container.addView(itemView);
        final int cPos = position;
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imageView= (ImageView) view;
                if(cPos==0)
                {

                }
            }
        });
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((LinearLayout)object);
    }
}
