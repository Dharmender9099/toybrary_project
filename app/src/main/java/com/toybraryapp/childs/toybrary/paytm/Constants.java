package com.toybraryapp.childs.toybrary.paytm;

/**
 * Created by Dharmender on 18-01-2018.
 */

public class Constants {
    public static final String M_ID = "OLATRA97893410082062"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WEB"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail109"; //Paytm industry type got it in paytm credential

    public static final String WEBSITE = "OLATRAWEB";
    public static final String CALLBACK_URL = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
}
