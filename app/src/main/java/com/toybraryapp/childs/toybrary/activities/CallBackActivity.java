package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

public class CallBackActivity extends AppCompatActivity {
EditText name,mobile,message;
Button send;
String errormsg="";
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_back);
        name=(EditText)findViewById(R.id.name);
        mobile=(EditText)findViewById(R.id.mobile);
        message=(EditText)findViewById(R.id.message);
        send=(Button)findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack();
            }
        });
    }
    private void callBack()
    {

        final String user_name=name.getText().toString();
        final String user_mobile=mobile.getText().toString();
        final String user_message=message.getText().toString();

        if(TextUtils.isEmpty(user_name))
        {
            name.setError("please enter name");
            name.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(user_mobile))
        {
            mobile.setError("please enter mobileno");
            mobile.requestFocus();
            return;
        }

        if(mobile.length()>10)
        {
            mobile.setError("please enter valid mobileno");
            mobile.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(user_message))
        {
            message.setError("please enter any query");
            message.requestFocus();
            return;
        }

        progressDialog = ProgressDialog.show(this, "", "Please wait...");
        StringRequest stringRequest=new StringRequest(StringRequest.Method.POST, URLs.CALLBACK_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                String res=response.trim();
             if(res.equals("Mail sent successfully"))
             {
                 startActivity(new Intent(CallBackActivity.this,MainActivity.class));
                 Toast.makeText(getApplicationContext(),"Mail sent successfully",Toast.LENGTH_LONG).show();

             }
             else
             {
                 Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
             }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("name",user_name);
                params.put("mobile",user_mobile);
                params.put("message",user_message);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 80000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}
