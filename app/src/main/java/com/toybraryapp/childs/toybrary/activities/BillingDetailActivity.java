package com.toybraryapp.childs.toybrary.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.toybraryapp.childs.toybrary.R;

public class BillingDetailActivity extends AppCompatActivity implements View.OnClickListener{
EditText address,postalcode,city,landmark;
TextView state,country,deliverytime;
String item[];
Button next;
LinearLayout state_layout,country_layout,deliverytime_layout;
String packageid,amount,promocode;
    private String stateArray[]={"West Bengal","Uttar Pradesh","Tripura","Tamil Nadu","Sikkim","Rajasthan","Punjab","Pondicherry","Orissa"
            ,"Nagaland","Mizoram","Meghalaya","Manipur","Maharashtra","Madhya Pradesh","Lakshadweep islands","Kerala","Karnataka","Jammu and Kashmir",
            "Himachal Pradesh","Haryana","Gujarat","Goa","Delhi","Daman and Diu","Dadra and Nagar Haveli","Chandigarh","Bihar","Assam","Arunchal Pradesh",
            "Andhra Pradesh","Andaman and Nicobar islands"};
    private String countryArray[]={"India"};
    private String deliverytimeArray[]={"Anytime between 9am and 5pm","Between 9am and 1pm","Between 1pm and 5pm"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_detail);
        address=(EditText)findViewById(R.id.address);
        postalcode=(EditText)findViewById(R.id.postalcode);
        state=(TextView)findViewById(R.id.state);
        state_layout=(LinearLayout)findViewById(R.id.state_layout);
        country=(TextView) findViewById(R.id.country);
        country_layout=(LinearLayout)findViewById(R.id.country_layout);
        city=(EditText)findViewById(R.id.city);
        landmark=(EditText)findViewById(R.id.landmark);
        deliverytime=(TextView)findViewById(R.id.deliverytime);
        deliverytime_layout=(LinearLayout)findViewById(R.id.deliverytime_layout);
        next=(Button)findViewById(R.id.next);
        next.setOnClickListener(this);
        state_layout.setOnClickListener(this);
        deliverytime_layout.setOnClickListener(this);
        country_layout.setOnClickListener(this);
         packageid=getIntent().getStringExtra("packageid");
         promocode=getIntent().getStringExtra("promocode");
         amount=getIntent().getStringExtra("depositamount");
    }
    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.state_layout:
               onClickState();
               break;
            case R.id.country_layout:
                onClickCountry();
                break;
            case R.id.deliverytime_layout:
                onClickDeliveryTime();
                break;

            case R.id.next:
                bellingDetail();
        }

    }

    public void bellingDetail()
    {
     String maddress=address.getText().toString();
     String mpostalcode=postalcode.getText().toString();
     String mcity=city.getText().toString();
     String mlandmark=landmark.getText().toString();
     String mstate=state.getText().toString();
     String mcountry=country.getText().toString();


     if(TextUtils.isEmpty(maddress))
     {
         address.setError("please enter address");
         address.requestFocus();
         return;
     }
     if(TextUtils.isEmpty(mpostalcode))
     {
         postalcode.setError("please enter postal code");
         postalcode.requestFocus();
         return;
     }
     if(TextUtils.isEmpty(mcity))
     {
         city.setError("please entery city name");
         city.requestFocus();
         return;
     }
     if(TextUtils.isEmpty(mlandmark))
     {
         landmark.setError("please enter landmark details");
         landmark.requestFocus();
         return;
     }

        item= new String[]{packageid,promocode,amount,maddress,mcountry,mstate,mcity,mpostalcode};
        Intent intent=new Intent(BillingDetailActivity.this,ShippingDetialsActivity.class);
        intent.putExtra("billingData",item);
        startActivity(intent);
    }
    public void onClickState()
    {

        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Please select any state");
        builder.setSingleChoiceItems(stateArray, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                state.setText(stateArray[i].toString());
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
    public void onClickCountry()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Please select any country");
        builder.setSingleChoiceItems(countryArray, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                country.setText(countryArray[i].toString());
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
    public void onClickDeliveryTime()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Please select any delivery time");
        builder.setSingleChoiceItems(deliverytimeArray, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deliverytime.setText(deliverytimeArray[i].toString());
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }


}
