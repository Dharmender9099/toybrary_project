package com.toybraryapp.childs.toybrary.volleyRequest;

import com.android.volley.VolleyError;

import org.json.JSONArray;

/**
 * Created by Dharmender on 19-02-2018.
 */

public interface VolleyResponse {
    public void notifySuccess(int requestType,JSONArray response);
    public void notifyError(int requestType,VolleyError error);
}
