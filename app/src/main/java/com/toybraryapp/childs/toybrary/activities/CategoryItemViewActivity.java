package com.toybraryapp.childs.toybrary.activities;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.GridAdapter;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.CartItem;
import com.toybraryapp.childs.toybrary.util.Counter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryItemViewActivity extends AppCompatActivity {
    RecyclerView getItemRecyclerView;
    GridAdapter gridAdapter;
    LinearLayout linearLayout1;
    GridLayoutManager manager;
    private List<Favourites> selectItem;
    String productId;
    String productBrand;
    String name;
    String errormsg="";
    int n=0;
    TextView txtViewCount;
    Counter counter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_item_view);
        ActionBar actionBar = getSupportActionBar(); // or getActionBar();
        linearLayout1=(LinearLayout)findViewById(R.id.linearlayout1);
        Bundle intent=getIntent().getExtras();
        productId=intent.getString("getId");
        name=intent.getString("points_range");
        productBrand=intent.getString("getFilterName");
        if(productId.equals("") && productBrand.equals(""))
        {
            productId="";
            productBrand="";
        }
        Toast.makeText(getApplicationContext(),name,Toast.LENGTH_LONG).show();
        getItemRecyclerView=(RecyclerView)findViewById(R.id.getItemRecyclerView);
        manager = new GridLayoutManager(this, 2);
       getItemDetials();
        itemInCart();
        counter=new Counter() {
            @Override
            public void setCounter(int count) {
                n=n+count;
                invalidateOptionsMenu();
            }
        };
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.addcart);
        CartItem cartItem=new CartItem(this);
        cartItem.totalItem();
        MenuItemCompat.setActionView(menuItem, R.layout.cart_icon_toolbar);
        RelativeLayout mycarttoolbar = (RelativeLayout) MenuItemCompat.getActionView(menuItem);
        RelativeLayout relativeLayout=(RelativeLayout)mycarttoolbar.findViewById(R.id.relative_layout_item_count);
        txtViewCount = (TextView) mycarttoolbar.findViewById(R.id.badge_notification_1);
        if(n!=0) {
            txtViewCount.setVisibility(View.VISIBLE);
            txtViewCount.setText(String.valueOf(n));
        }
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CategoryItemViewActivity.this, ActivityMy_Cart.class));
            }
        });
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.contactus) {
            startActivity(new Intent(CategoryItemViewActivity.this, ContactUsActivity.class));
        } else if (id == R.id.logout) {
            SharedPreferences preferences =getSharedPreferences("myPref1",MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
            finish();
        }
        else if(id==R.id.editprofile)
        {
            startActivity(new Intent(this,EditProfileActivity.class));
        }
        else if(id==R.id.changepass)
        {
            startActivity(new Intent(this,ChangePasswordActivity.class));
        }
        else if(id==R.id.privacy)
        {
            startActivity(new Intent(this,PolicyActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void getItemDetials()
    {
        StringRequest stringRequest=new StringRequest(Request.Method.POST,URLs.SUBCATEGORY_ITEM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               selectItem=new ArrayList<>();
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray packages=jsonObject.getJSONArray("product_details");
                    for(int i=0;i<packages.length();i++) {
                        Favourites gallery=new Favourites();
                        JSONObject obj=packages.getJSONObject(i);
                        gallery.setId(obj.getString("productsId"));
                        gallery.setImage(URLs.SEARCH_IMAGE_PATH+obj.getString("productsPhoto"));
                        gallery.setName(obj.getString("productsName"));
                        gallery.setNumber(obj.getString("productsPoints"));
                        gallery.setStock(obj.getString("productStatus"));
                        selectItem.add(gallery);
                    }

                }
                catch(JSONException e)
                {
                    e.printStackTrace();
                }
                if(selectItem.size()>0) {
                    gridAdapter = new GridAdapter(CategoryItemViewActivity.this, selectItem, counter);
                    getItemRecyclerView.setLayoutManager(manager);
                    getItemRecyclerView.setAdapter(gridAdapter);
                    gridAdapter.notifyDataSetChanged();
                }
                else
                {
                    Snackbar snackbar = Snackbar.make(linearLayout1, "Sorry no record found!", Snackbar.LENGTH_LONG);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("productId",productId);
                map.put("productBrand",productBrand);
                map.put("points_range",name);
                return map;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
    public void itemInCart() {
        SessionManager sessionManager = new SessionManager(CategoryItemViewActivity.this);
        final String userId = sessionManager.getUserid();
        if (!userId.isEmpty()) {
            if (!userId.isEmpty()) {
                StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        String res = response.trim();
                        String totalitem = res;
                        n = Integer.parseInt(totalitem);
                        if(n!=0) {
                            txtViewCount.setVisibility(View.VISIBLE);
                            txtViewCount.setText(totalitem);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof TimeoutError || error instanceof NoConnectionError) {
                            errormsg="No internet connection.";
                        }
                        else if (error instanceof AuthFailureError) {
                            errormsg="Authentication Failure.";
                        }
                        else if (error instanceof ServerError) {
                            errormsg="Server down.";
                        }
                        else if (error instanceof ParseError) {
                            errormsg="problem in fetching data.";
                        }
                        Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();
                        params.put("user_id", userId);
                        params.put("action", "total_cart_item");
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(stringRequest);
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        itemInCart();
        invalidateOptionsMenu();
    }
}
