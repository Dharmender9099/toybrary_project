package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity {
EditText oldpass,newpass,verifypass;
 private String userid,errormsg;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        oldpass=(EditText)findViewById(R.id.current);
        newpass=(EditText)findViewById(R.id.newpass);
        verifypass=(EditText)findViewById(R.id.verify);
        getSupportActionBar().setTitle("Change Password");
        SessionManager sessionManager=new SessionManager(this);
        userid=sessionManager.getUserid();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_accept, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.action_settings) {

               changepass();
        }
        return super.onOptionsItemSelected(item);
    }
    public void  changepass()
    {
        final String oldpassword=oldpass.getText().toString();
        final String newpassword=newpass.getText().toString();
        final String verifypassword=verifypass.getText().toString();
        if(oldpassword.isEmpty())
        {
            oldpass.setError("please enter current current password");
            oldpass.requestFocus();
            return;
        }
        if(newpassword.isEmpty())
        {
            newpass.setError("please enter new password");
            newpass.requestFocus();
            return;
        }
        if(verifypassword.isEmpty())
        {
            verifypass.setError("please enter confirm password");
            verifypass.requestFocus();
            return;
        }
        if(!verifypassword.equals(newpassword))
        {
            verifypass.setError("password does not match");
            verifypass.requestFocus();
            return;
        }
       // progressDialog = ProgressDialog.show(getApplicationContext(), "", "Please wait..");
        StringRequest changepassreq=new StringRequest(StringRequest.Method.POST, URLs.CHANGEPASS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
             String res=response.trim();
           //  progressDialog.dismiss();
             if(res.equals("Your Password is changed succesfully"))
             {
                 Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
                 startActivity(new Intent(ChangePasswordActivity.this,MainActivity.class));
             }
             else
             {
                 Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
             }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  progressDialog.dismiss();
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams(){
                Map<String,String> map=new HashMap<>();
                map.put("userid",userid);
                map.put("oldpassword",oldpassword);
                map.put("password",verifypassword);
                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(changepassreq);
    }
}