package com.toybraryapp.childs.toybrary.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.toybraryapp.childs.toybrary.R;

/**
 * Created by Dharmender on 07-11-2017.
 */

public class TabFragment extends Fragment{
   static int count=2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tablayout_fragment,container,false);
        TabLayout tabLayout=(TabLayout)view.findViewById(R.id.tablayout);
        ViewPager viewPager=(ViewPager)view.findViewById(R.id.viewpager);
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {

            }
        });
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    public class MyAdapter extends FragmentPagerAdapter
    {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position)
            {
                case 0:
                    return new LoginFragment();
                case 1:
                    return new RegisterFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position)
            {
                case 0:
                    return "SIGN IN";
                case 1:
                    return "REGISTER";
            }
            return null;
        }
    }
}
