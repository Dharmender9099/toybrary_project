package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.activities.ActivityMy_Cart;
import com.toybraryapp.childs.toybrary.activities.AppController;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.activities.SingleProductActivity;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.PointsCounter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dharmender on 15-11-2017.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder>{
      Context context;
      PointsCounter counter;
      int count=1;
      String userId;
      List<Favourites> list=new ArrayList();
      ImageLoader imageLoader = AppController.getInstance().getImageLoader();
      public CartAdapter(Context context, List<Favourites> list, PointsCounter counter) {
      this.context=context;
      this.list=list;
      this.counter=counter;
          SessionManager sessionManager=new SessionManager(context);
          userId=sessionManager.getUserid();
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.addcart_layout,parent,false);
        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CartViewHolder holder, final int position) {
        Favourites favourites=list.get(position);
        holder.product_id.setText(favourites.getId());
        holder.productImage.setImageUrl(favourites.getImage(),imageLoader);
        holder.productName.setText(favourites.getName());
        String point=favourites.getPoints();
        int p= (int) Double.parseDouble(point);
        holder.productPoints.setText(String.valueOf(p));
        holder.productQuantity.setText(favourites.getQuantity());
        int tp= (int) Double.parseDouble(favourites.getTotalpoints());
        if(tp==0)
        {
         holder.totalpoints.setText(String.valueOf(p));
        }
        else {
            holder.totalpoints.setText(String.valueOf(tp));
        }
        holder.addProduct.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             String pnts=holder.productQuantity.getText().toString();
             int point=Integer.parseInt(holder.productPoints.getText().toString());
             counter.setpointCounter(point,"add");
             count=Integer.parseInt(pnts);
             count++;
             holder.productQuantity.setText(String.valueOf(count));
             holder.totalpoints.setText(String.valueOf(count*point));
             String add="update_quantity";
             String productId=holder.product_id.getText().toString();
             String qnt=holder.productQuantity.getText().toString();
             String tpoints=holder.totalpoints.getText().toString();
             updateCart( productId,userId,add,qnt,tpoints);
          }
     });

     holder.removeProduct.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             String pnts=holder.productQuantity.getText().toString();
             count=Integer.parseInt(pnts);
             count--;
            if(count<1)
             {
                 holder.productQuantity.setText("1");
                 count=1;
             }
             else {
                int point=Integer.parseInt(holder.productPoints.getText().toString());
                counter.setpointCounter(point,"remove");
                 holder.productQuantity.setText(String.valueOf(count));
                holder.totalpoints.setText(String.valueOf(count*point));
                String add="update_quantity";
                String productId=holder.product_id.getText().toString();
                String qnt=holder.productQuantity.getText().toString();
                String tpoints=holder.totalpoints.getText().toString();
                updateCart( productId,userId,add,qnt,tpoints);
             }

         }
     });
     holder.removeitemfromcart.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             String add="deleteitem";
             SessionManager sessionManager=new SessionManager(context);
             String userId=sessionManager.getUserid();
             String productId=holder.product_id.getText().toString();
             updateCart( productId,userId,add,"null","null");
             list.remove(position);
             notifyItemRemoved(position);
             notifyItemRangeChanged(position, list.size());
             int tps=Integer.parseInt(holder.totalpoints.getText().toString());
             counter.setpointCounter(tps,"deleteItem");
             context.startActivity(new Intent(context,ActivityMy_Cart.class));

         }
     });
     holder.cardview.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             String product_id=holder.product_id.getText().toString();
             Intent intent=new Intent(context, SingleProductActivity.class);
             intent.putExtra("productId",product_id);
             context.startActivity(intent);
         }
     });
    }
    public void updateCart(final String productid, final String userid,final String additem,final String quantity,final String totalpoints)
    {
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                if(res.equals("product removed from cart")) {

                    Toast.makeText(context, response, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,String.valueOf(error),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("product_id",productid);
                params.put("user_id",userid);
                params.put("action",additem);
                params.put("quantity",quantity);
                params.put("totalpoints",totalpoints);
                return params;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder
    {
        TextView productQuantity,productName,productPoints,product_id,totalpoints;
        ImageButton removeitemfromcart;
        NetworkImageView productImage;
        ImageView removeProduct,addProduct;
        CardView cardview;

        public CartViewHolder(View itemView) {
            super(itemView);
            product_id=(TextView)itemView.findViewById(R.id.product_id);
            productImage=(NetworkImageView)itemView.findViewById(R.id.productImage);
            productName=(TextView)itemView.findViewById(R.id.productName);
            productPoints=(TextView)itemView.findViewById(R.id.productPoints);
            addProduct=(ImageView)itemView.findViewById(R.id.addProduct);
            removeProduct=(ImageView)itemView.findViewById(R.id.removeProduct);
            productQuantity=(TextView)itemView.findViewById(R.id.productQuantity);
            removeitemfromcart=(ImageButton)itemView.findViewById(R.id.removeitemfromcart);
            totalpoints=(TextView)itemView.findViewById(R.id.totalpoints);
            cardview=(CardView)itemView.findViewById(R.id.cardview);

        }
    }
}
