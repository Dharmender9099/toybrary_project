package com.toybraryapp.childs.toybrary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.PackagesAdapter;
import com.toybraryapp.childs.toybrary.model.Gallery;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.UserDetails;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PackagesActivity extends AppCompatActivity {
    RecyclerView packageRecyclerview;
    RecyclerView.LayoutManager layoutManager;
    PackagesAdapter packageAdapter;
    TextView depamount;
    List<Gallery> list;
    Button buynow;
    String errormsg="";
    EditText promocode;
    public static String packageid;
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packages);
        getSupportActionBar().setTitle("Packages");
        buynow=(Button)findViewById(R.id.buynow);
        depamount=(TextView)findViewById(R.id.depamount);
        promocode=(EditText)findViewById(R.id.promo_code);
        packageRecyclerview=(RecyclerView)findViewById(R.id.packageRecyclerview);
        layoutManager=new LinearLayoutManager(this);
        getdata();
        buynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager=new SessionManager(PackagesActivity.this);
                Boolean isLogin=sessionManager.isLogin();
                if(isLogin) {
                    UserDetails userDetails=new UserDetails();
                    userDetails.setPromocode(promocode.getText().toString());
                    String depositamount = depamount.getText().toString();
                    String pcode=promocode.getText().toString();
                    Intent intent = new Intent(PackagesActivity.this, BillingDetailActivity.class);
                    intent.putExtra("packageid", packageid);
                    intent.putExtra("promocode",pcode);
                    intent.putExtra("depositamount", depositamount);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"please register first!",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(PackagesActivity.this,FragmentActivity.class));
                }
            }
        });
    }

    public void getdata()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,URLs.PACKAGE_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                list = new ArrayList<>();
                    try {
                     JSONObject jsonObject=new JSONObject(response);
                     JSONArray packages=jsonObject.getJSONArray("packages");
                        for(int i=0;i<packages.length();i++) {
                            Gallery packageItem = new Gallery();
                            JSONObject data=packages.getJSONObject(i);
                            packageItem.setId(data.getString("productId"));
                            packageItem.setTitle(data.getString("productName"));
                            packageItem.setValidity(data.getString("productValidity"));
                            packageItem.setPoints(data.getString("productPoints"));
                            packageItem.setPrice(data.getString("productPrice"));
                            list.add(packageItem);
                        }
                        packageAdapter=new PackagesAdapter(PackagesActivity.this,list);
                        packageRecyclerview.setItemAnimator(new DefaultItemAnimator());
                        packageRecyclerview.setLayoutManager(layoutManager);
                        packageAdapter.notifyDataSetChanged();
                        packageRecyclerview.setAdapter(packageAdapter);

                        JSONArray depositAmount=jsonObject.getJSONArray("depositAmount");
                        JSONObject amount=depositAmount.getJSONObject(0);
                         depamount.setText(amount.getString("depositAmount"));
                        } catch (JSONException e1) {
                        e1.printStackTrace();
                    }

                }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if( error instanceof NetworkError) {
                            errormsg="Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof AuthFailureError) {
                            errormsg="Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof ServerError) {
                            errormsg="The server could not be found. Please try again after some time!!";
                        }
                        else if (error instanceof ParseError) {
                            errormsg="Parsing error! Please try again after some time!!";
                        }
                        else if (error instanceof NoConnectionError) {
                            errormsg = "Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof TimeoutError) {
                            errormsg = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
                    }
                });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }
    }

