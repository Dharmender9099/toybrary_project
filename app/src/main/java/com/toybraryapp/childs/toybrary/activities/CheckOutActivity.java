package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CheckOutActivity extends AppCompatActivity {
    String userid,cart_totalpoints,ava_point;
    int points;
    TextView aval_points;
    Button ordernow;
    TextView username;
    ProgressDialog progressDialog;
    String errormsg,action;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        getSupportActionBar().setTitle("Place Order");
        username=(TextView)findViewById(R.id.username);
        SessionManager sessionManager=new SessionManager(this);
        username.setText(sessionManager.getUsername()+",");
        ordernow=(Button)findViewById(R.id.order_now);
        aval_points=(TextView)findViewById(R.id.available_points);
        ava_point = getIntent().getStringExtra("available_point");
        cart_totalpoints = getIntent().getStringExtra("cart_totalpoint");
        userid=sessionManager.getUserid();
        aval_points.setText(ava_point);
        ordernow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderNowReq();
            }
        });
    }

    public void orderNowReq()
    {
        progressDialog = ProgressDialog.show(CheckOutActivity.this, "", "Please wait..");
        StringRequest orderRequest=new StringRequest(Request.Method.POST, URLs.PAYMENT_PROCESS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
             String res=response.trim();
             if(res.equals("Your order successfully registered!")) {
                 Toast.makeText(getApplication(), res, Toast.LENGTH_LONG).show();
                 startActivity(new Intent(CheckOutActivity.this, ThankYouActivity.class));
             }
             else
             {
                 Toast.makeText(getApplication(), res, Toast.LENGTH_LONG).show();
             }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplication(),String.valueOf(error),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("userid",userid);
                map.put("cart_points",cart_totalpoints);
                map.put("available_point",ava_point);
                return map;
            }
        };
       /* int socketTimeout = 30000; // 15 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        orderRequest.setRetryPolicy(policy);*/
        AppController.getInstance().addToRequestQueue(orderRequest);
    }
    public void showData()
    {
        StringRequest stringRequest=new StringRequest(StringRequest.Method.POST, URLs.VIEW_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("cartItem");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        String tpoints = obj.getString("productTotalpoints");
                        points = (int) (points + Double.parseDouble(tpoints));
                    }


                    aval_points.setText(String.valueOf(points));
                }
                catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error instanceof TimeoutError || error instanceof NoConnectionError) {
                    errormsg="No internet connection.";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Authentication Failure.";
                }
                else if (error instanceof ServerError) {
                    errormsg="Server down.";
                }
                else if (error instanceof ParseError) {
                    errormsg="problem in fetching data.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("user_id",userid);
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

}
