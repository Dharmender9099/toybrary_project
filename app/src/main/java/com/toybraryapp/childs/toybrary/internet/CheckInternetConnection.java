package com.toybraryapp.childs.toybrary.internet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Dharmender on 13-11-2017.
 */

public class CheckInternetConnection {
    private Context context;
    public CheckInternetConnection(Context context)
    {
        this.context=context;
    }
    public  boolean isConnectTointernet()
    {
        ConnectivityManager connectivity= (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        if(connectivity !=null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)

                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

}
