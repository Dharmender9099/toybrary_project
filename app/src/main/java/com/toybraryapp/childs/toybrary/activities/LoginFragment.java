package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dharmender on 07-11-2017.
 */

public class LoginFragment extends Fragment {
    EditText email,password,forgotemail;
    TextView forgotpassword;
    Button signin;
    TextView skip;
    String msg,user_id,username;
    Context context;
    String errormsg="";
    private ProgressDialog progressDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.login_fragment,container,false);
        email=(EditText)view.findViewById(R.id.email);
        password=(EditText)view.findViewById(R.id.password);
        forgotpassword=(TextView)view.findViewById(R.id.forgotpassword);
        signin=(Button)view.findViewById(R.id.signin);
        skip=(TextView)view.findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),MainActivity.class));
                getActivity().finish();
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    userLogin();
            }
        });
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customAlertDialog();
            }
        });
        return view;
    }
    private void customAlertDialog()
    {
        LayoutInflater layoutInflater=LayoutInflater.from(getActivity());
        View viewItem=layoutInflater.inflate(R.layout.forgot_password_layout,null);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setView(viewItem);
        final AlertDialog alertDialog=builder.create();
        alertDialog.show();
        forgotemail=(EditText)viewItem.findViewById(R.id.forgot_email);
        TextView cancel_action=(TextView)viewItem.findViewById(R.id.cancel_action);
        TextView request_action=(TextView)viewItem.findViewById(R.id.request_action);

        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             alertDialog.dismiss();
            }
        });
        request_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPassword();
            }
        });

    }

    private void userLogin() {
        final String mail = email.getText().toString();
        final  String pass = password.getText().toString();

        if(TextUtils.isEmpty(mail))
        {
          email.setError("Please enter mail-id or mobileno");
          email.requestFocus();
          return;
        }


      /*  if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
            email.setError("Enter a valid email");
            email.requestFocus();
            return;
        }*/

        if(TextUtils.isEmpty(pass))
        {
            password.setError("please enter password");
            password.requestFocus();
            return;
        }
        progressDialog = ProgressDialog.show(getContext(), "", "Please wait...");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.LOGIN_URL,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                String res=response.trim();
                if(res.equals("Invalid Login Credentials"))
                {
                    Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                }
                else {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        JSONArray packages = jsonObject.getJSONArray("user_detail");
                        for (int i = 0; i < packages.length(); i++) {
                            JSONObject obj1 = packages.getJSONObject(i);
                            user_id = obj1.getString("userid");
                            username=obj1.getString("username");
                            msg = obj1.getString("msg");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(msg.equals("successfully login"))
                    {
                        SessionManager sessionManager=new SessionManager(getActivity());
                        sessionManager.createLoginSession(username,user_id);
                        Toast.makeText(getActivity(),"successfully login",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getActivity(),MainActivity.class));
                        getActivity().finish();
                    }
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if( error instanceof NetworkError) {
                            errormsg="Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof AuthFailureError) {
                            errormsg="Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof ServerError) {
                            errormsg="The server could not be found. Please try again after some time!!";
                        }
                        else if (error instanceof ParseError) {
                            errormsg="Parsing error! Please try again after some time!!";
                        }
                        else if (error instanceof NoConnectionError) {
                            errormsg = "Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof TimeoutError) {
                            errormsg = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getActivity(),errormsg,Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("mobile",mail);
                params.put("password",pass);
                return params;
            }

        };

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    public void forgotPassword()
    {
     final String mailId=forgotemail.getText().toString();
     if(TextUtils.isEmpty(mailId))
     {
         forgotemail.setError("please enter mail id or mobileno");
         forgotemail.requestFocus();
         return;
     }
       /* if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mailId).matches()) {
            forgotemail.setError("Enter a valid email-id");
            forgotemail.requestFocus();
            return;
        }*/
        progressDialog = ProgressDialog.show(getContext(), "", "Please wait...");
        StringRequest forgotmail=new StringRequest(Request.Method.POST, URLs.FORGOT_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
            String res=response.trim();
            if(res.equals("Incorrect Email or Mobile"))
            {
                Toast.makeText(getActivity(),"Email or mobile is not registered",Toast.LENGTH_LONG).show();
            }
            else if(res.equals("Please check your mail. Your Password is send in your Account."))
            {
                Toast.makeText(getActivity(),"Your Password is send in your Email",Toast.LENGTH_LONG).show();
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
            else
            {
                Toast.makeText(getActivity(),response,Toast.LENGTH_LONG).show();
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getActivity(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                SessionManager sessionManager=new SessionManager(getActivity());
                String userid=sessionManager.getUserid();
               Map<String,String> map=new HashMap<>();
               map.put("email",mailId);
               map.put("userid",userid);
               return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(forgotmail);
    }
}
