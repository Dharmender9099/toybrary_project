package com.toybraryapp.childs.toybrary.activities;


import android.content.Context;
import android.media.MediaCas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.MyPackageAdapter;
import com.toybraryapp.childs.toybrary.model.Gallery;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dharmender on 14-01-2018.
 */

public class MyPackage_Fragment extends Fragment {
    RecyclerView mypackage_recyclerview;
    String action="mypackage";
    RecyclerView.LayoutManager layoutManager;
    Context context;
    String userid;
    String errormsg;
    List<Gallery> list;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.mypackage_fragment,container,false);
        SessionManager sessionManager=new SessionManager(getActivity());
        userid=sessionManager.getUserid();
        mypackage_recyclerview=(RecyclerView)view.findViewById(R.id.mypackage_recyclerview);
        layoutManager=new LinearLayoutManager(context);
        mypackage();
        return view;
    }
    public void mypackage()
    {
        StringRequest mypackageReq=new StringRequest(StringRequest.Method.POST, URLs.MYACCOUNT_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                list=new ArrayList<>();
                JSONObject jsonObject= null;
                try {
                    jsonObject = new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("mypackage");
                    for(int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject obj= (JSONObject) jsonArray.get(i);
                        Gallery gallery=new Gallery();
                        gallery.setName(obj.getString("package_title"));
                        gallery.setPrice(obj.getString("package_price"));
                        gallery.setValidity(obj.getString("package_validity"));
                        gallery.setPoints(obj.getString("package_points"));
                        gallery.setStatus(obj.getString("package_status"));
                        gallery.setExpiredate(obj.getString("package_expire_date"));
                        gallery.setPurchasedate(obj.getString("package_purchase_date"));
                        list.add(gallery);
                    }
                    MyPackageAdapter myPackageAdapter=new MyPackageAdapter(getActivity(),list);
                    mypackage_recyclerview.setItemAnimator(new DefaultItemAnimator());
                    mypackage_recyclerview.setLayoutManager(layoutManager);
                    mypackage_recyclerview.setAdapter(myPackageAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";

                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(context,errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("action",action);
                map.put("userid",userid);
                return map;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(mypackageReq);
    }
}
