package com.toybraryapp.childs.toybrary.activities;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.SurfaceView;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.toybraryapp.childs.toybrary.R;

public class URLVideoActivity extends AppCompatActivity {
VideoView fullvideo;
    DisplayMetrics dm;
    SurfaceView sur_View;
    MediaController media_Controller;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_urlvideo);
        fullvideo=(VideoView)findViewById(R.id.full_video);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        String videopath=getIntent().getStringExtra("uri");
        Uri videoUri = Uri.parse(videopath);
        fullvideo.setVideoURI(videoUri);
        fullvideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
               // mp.setLooping(true);
                media_Controller = new MediaController(URLVideoActivity.this);
                    dm = new DisplayMetrics();
                progressBar.setVisibility(View.GONE);
                    int height = dm.heightPixels;
                    int width = dm.widthPixels;
                    fullvideo.setMinimumWidth(width);
                    fullvideo.setMinimumHeight(height);
                    fullvideo.setMediaController(media_Controller);
                    fullvideo.start();
            }
        });
        fullvideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                fullvideo.stopPlayback();
                Toast.makeText(getApplicationContext(),"Thank you for watching.",Toast.LENGTH_LONG).show();
            }
        });

    }
}
