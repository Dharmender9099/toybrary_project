package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.toybraryapp.childs.toybrary.activities.CategoryItemViewActivity;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dharmender on 03-12-2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    Context context;
    List<Favourites> list=new ArrayList<>();
     OnLoadMoreListener onLoadMoreListener;
    public CategoryAdapter(RecyclerView recyclerView,Context context,List<Favourites> list)
    {
        this.context=context;
        this.list=list;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
      /*  recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
               // totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)){
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });*/
        isLoading = true;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position)==null?VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType==VIEW_TYPE_ITEM) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.view_layout, null);
            return new CategoryHolder(view);
        }
        else if(viewType==VIEW_TYPE_LOADING)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.layout_loading_item, null);
            return new CategoryHolder(view);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CategoryHolder) {
            Favourites favourites = list.get(position);
            final CategoryHolder categoryHolder=(CategoryHolder) holder;
            categoryHolder.product_id.setText(favourites.getId());
            categoryHolder.identity.setText(favourites.getIdentity());
            categoryHolder.item.setText(favourites.getName());
            categoryHolder.category_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String selectItemId = categoryHolder.product_id.getText().toString();
                    String brands = categoryHolder.identity.getText().toString();
                    String name =categoryHolder.item.getText().toString();
                    Intent intent = new Intent(context, CategoryItemViewActivity.class);
                    intent.putExtra("getId", selectItemId);
                    intent.putExtra("getFilterName", brands);
                    intent.putExtra("points_range", name);
                    context.startActivity(intent);
                }
            });
        }
        else if(holder instanceof LoadingViewHolder)
        {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }
    public void setLoaded() {
        isLoading = false;
    }

    @Override
    public int getItemCount() {
        return list==null? 0:list.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder{
        TextView item,product_id,identity;
        LinearLayout category_layout;
        public CategoryHolder(View itemView) {
            super(itemView);
            item=(TextView)itemView.findViewById(R.id.item);
            product_id=(TextView)itemView.findViewById(R.id.product_id);
            identity=(TextView)itemView.findViewById(R.id.identity);
            category_layout=(LinearLayout)itemView.findViewById(R.id.category_layout);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}
