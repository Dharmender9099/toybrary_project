package com.toybraryapp.childs.toybrary.activities;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;

import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.AllCategoryAdapter;
import com.toybraryapp.childs.toybrary.adapter.CatalogueAdapter;
import com.toybraryapp.childs.toybrary.adapter.FavouritesAdapter;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.OnLoadMoreListener;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.subCategoryList.ShowCategoryAdapeter;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.Counter;
import com.toybraryapp.childs.toybrary.volleyRequest.VolleyResponse;
import com.toybraryapp.childs.toybrary.volleyRequest.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class MainActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener{
    String userid;
    GridLayoutManager manager;
    DrawerLayout drawerLayout;
    ImageView userProfile;
    private String TAG = MainActivity.class.getSimpleName();
   public static ArrayList<CategoryHolder> arrayList1;
    ArrayList<String> toys1;
    LinearLayout catalogue, feedback, gallery, callback, brandslayout, agelayout, pointlayout, aboutus, howitworks,
            suggesttoy, monthlypackage,myaccount,facebooklayout,youtube,instagram,twitter,linkedin,myorder,mycart;
    String errormsg="";
    private Counter counter;
    private Helper listView;
    TextView navheader;
    TextView morefavourites, morenewly,txtViewCount;
    CardView searchtoys;
    int count=0;
    public List<Favourites> favouritesItemList, newlyItemList,homecategorylist;
    public static ArrayList<CategoryHolder> arrayList;
    NestedScrollView nestedScrollView;
    public static HashMap<String,ArrayList<String>> hashMap;
    CardView signin;
    DrawerLayout drawer;
    RecyclerView favouritesRecyclerView,allcategoryRecyclerview;
    RecyclerView newlyRecyclerView;
    SessionManager sessionManager;
    List<String> list=new ArrayList<>();
    RecyclerView.LayoutManager layoutManager, layoutManager2;
    int n;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);

        navheader=(TextView)findViewById(R.id.navheader);
        sessionManager=new SessionManager(this);
        signin=(CardView)findViewById(R.id.signin);
        toys1=new ArrayList<>();

        // check user register or not

        Boolean isLogin=sessionManager.isLogin();
        userid=sessionManager.getUserid();
        if(isLogin)
        {
            getCartTotalItem();
            String username=sessionManager.getUsername();
            navheader.setText("Hello"+", "+username);
            signin.setVisibility(View.GONE);
        }
        else
        {
            signin.setVisibility(View.VISIBLE);
        }
        morefavourites = (TextView) findViewById(R.id.morefavourites);
        morenewly = (TextView) findViewById(R.id.morenewly);
        favouritesRecyclerView = (RecyclerView) findViewById(R.id.favouritesRecyclerView);
        newlyRecyclerView = (RecyclerView) findViewById(R.id.newlyRecyclerView);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        //implement callback interface

       counter=new Counter() {
           @Override
             public void setCounter(int count) {
                    n=n+count;
                    invalidateOptionsMenu();
           }
       };
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        nestedScrollView = (NestedScrollView) navigationView.findViewById(R.id.scrollposition);
        refrenceNavigationCategory(navigationView);
        listView = (Helper) navigationView.findViewById(R.id.parentcategoryList);
        morefavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MoreFavourite_NewlyActivity.class);
                intent.putExtra("getvalue", "favourites");
                startActivity(intent);
            }
        });
        morenewly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MoreFavourite_NewlyActivity.class);
                intent.putExtra("getvalue", "newly");
                startActivity(intent);
            }
        });
        userProfile=(ImageView)findViewById(R.id.user_profile);
        userProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,EditProfileActivity.class));
            }
        });
        initiaze();
        categoryItem();
        favouriteItem();
        newlyImage();
        getAllCategoryData();
    }

    // get all category of product

    public void categoryItem()
    {
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(URLs.CATEGORY, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                arrayList1=new ArrayList();
                hashMap = new HashMap<>();
                for(int i=0;i<response.length();i++)
                {
                    try
                    {
                        JSONObject obj=response.getJSONObject(i);
                        String cat_name=obj.getString("categoryName");
                        String cat_id=obj.getString("categoryId");
                        String p_id=obj.getString("categoryParentId");
                        if(p_id.equals("0"))
                        {
                            arrayList1.add(new CategoryHolder(cat_name,0,cat_id));
                        }
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                int s1=arrayList1.size();
                for(int i=1;i<=s1;i++)
                {
                    subCategoryItem(String.valueOf(i));

                }
                navigationCategoryList();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);

    }

    // set subcategory item in expandable list

    public void subCategoryItem(final String  id)
    {
        final List<String> child=new ArrayList<>();
        StringRequest stringRequest=new StringRequest(Request.Method.POST,URLs.SUB_CATEGORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray packages=jsonObject.getJSONArray("item_details");
                    for(int i=0;i<packages.length();i++) {
                        JSONObject obj=packages.getJSONObject(i);
                        String cat_name=obj.getString("categoryName");
                        child.add(cat_name);
                    }
                }
                catch(JSONException e)
                {
                    e.printStackTrace();
                }
                hashMap.put(arrayList1.get(Integer.parseInt(id)-1).getPARENTCATEGORY(), (ArrayList<String>) child);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error instanceof TimeoutError || error instanceof NoConnectionError) {
                    errormsg="No internet connection.";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Authentication Failure.";
                }
                else if (error instanceof ServerError) {
                    errormsg="Server down.";
                }
                else if (error instanceof ParseError) {
                    errormsg="problem in fetching data.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("sub_cat_id",String.valueOf(id));
                return map;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void initiaze() {
        allcategoryRecyclerview=(RecyclerView)findViewById(R.id.allcategory);
        manager = new GridLayoutManager(this, 2);
        allcategoryRecyclerview.setLayoutManager(manager);

        arrayList = new ArrayList<>();
        hashMap = new HashMap<>();
    }

    public void navigationCategoryList() {
        final ShowCategoryAdapeter showCategoryAdapeter = new ShowCategoryAdapeter(MainActivity.this, arrayList1, hashMap, listView);
        listView.setAdapter(showCategoryAdapeter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CategoryHolder categoryHolder=(CategoryHolder) adapterView.getItemAtPosition(i);
                Toast.makeText(getApplicationContext(),categoryHolder.getParentId(),Toast.LENGTH_LONG).show();
            }
        });
       listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (arrayList1.get(groupPosition).getFLAG_INDICATOR() == 1) {
                    listView.collapseGroup(groupPosition);
                    arrayList1.get(groupPosition).setFLAG_INDICATOR(0);

                } else {
                    for (int i = 0; i < arrayList1.size(); i++) {
                        if (arrayList1.get(i).getFLAG_INDICATOR() == 1) {
                            listView.collapseGroup(i);
                            arrayList1.get(i).setFLAG_INDICATOR(0);
                        }

                    }
                    listView.expandGroup(groupPosition);
                    listView.setSelectedGroup(groupPosition);
                    nestedScrollView.smoothScrollTo(0, groupPosition);
                    arrayList1.get(groupPosition).setFLAG_INDICATOR(1);

                }
                return true;
            }
        });

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Intent intent = new Intent(MainActivity.this, SubCategoryItemViewActivity.class);
                intent.putExtra("get", hashMap.get(arrayList1.get(groupPosition).getPARENTCATEGORY()).get(childPosition));
                intent.putExtra("id","");
                startActivity(intent);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                for (int i = 0; i < parent.getCount(); ++i) {
                    if (parent.isGroupExpanded(i)) {
                        parent.collapseGroup(i);
                    }
                }

                return true;
            }
        });

    }

    // onBackPressed

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
           // super.onBackPressed();
            exitapp();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.addcart);
        MenuItemCompat.setActionView(menuItem, R.layout.cart_icon_toolbar);
        RelativeLayout mycarttoolbar = (RelativeLayout) MenuItemCompat.getActionView(menuItem);
        RelativeLayout relativeLayout=(RelativeLayout)mycarttoolbar.findViewById(R.id.relative_layout_item_count);
        txtViewCount = (TextView) mycarttoolbar.findViewById(R.id.badge_notification_1);
        if(n!=0) {
            txtViewCount.setVisibility(View.VISIBLE);
            txtViewCount.setText(String.valueOf(n));
        }
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ActivityMy_Cart.class));
            }
        });
        return true;
    }
 /* public void updateHotCount(int new_hot_number) {
      LayoutInflater inflater = LayoutInflater.from(this);
      View view = inflater.inflate(R.layout.cart_icon_toolbar, null);
        count = new_hot_number;
      TextView textView=(TextView)view.findViewById(R.id.badge_notification_1);
        if (count > 0) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (count == 0)
                    textView.setVisibility(View.GONE);
                else {
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(String.valueOf(count));
                    invalidateOptionsMenu();
                }
            }
        });
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.contactus) {
            startActivity(new Intent(MainActivity.this, ContactUsActivity.class));
        } else if (id == R.id.logout) {
            SharedPreferences preferences =getSharedPreferences("myPref1",MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
            finish();
            startActivity(new Intent(MainActivity.this,FragmentActivity.class));
        } else if (id == R.id.addcart) {
            startActivity(new Intent(MainActivity.this, ActivityMy_Cart.class));
        }
        else if(id==R.id.editprofile)
        {
            startActivity(new Intent(MainActivity.this,EditProfileActivity.class));
        }
        else if(id==R.id.changepass)
        {
            startActivity(new Intent(MainActivity.this,ChangePasswordActivity.class));
        }
        else if(id==R.id.privacy)
        {
            startActivity(new Intent(MainActivity.this,PolicyActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.callbacklayout) {
            startActivity(new Intent(MainActivity.this, CallBackActivity.class));
        } else if (id == R.id.feedbacklayout) {
            startActivity(new Intent(MainActivity.this, FeedbackActivity.class));

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void refrenceNavigationCategory(View view) {
        catalogue = (LinearLayout) findViewById(R.id.cataloguelayout);
        gallery = (LinearLayout) findViewById(R.id.gallerylayout);
        callback = (LinearLayout) view.findViewById(R.id.callbacklayout);
        feedback = (LinearLayout) findViewById(R.id.feedbacklayout);
        brandslayout = (LinearLayout) findViewById(R.id.brandslayout);
        agelayout = (LinearLayout) findViewById(R.id.agelayout);
        pointlayout = (LinearLayout) findViewById(R.id.pointlayout);
        aboutus = (LinearLayout) findViewById(R.id.aboutus);
        howitworks = (LinearLayout) findViewById(R.id.howitwork);
        suggesttoy = (LinearLayout) findViewById(R.id.suggesttoylayout);
        linkedin=(LinearLayout)findViewById(R.id.linkedinlayout);
        mycart=(LinearLayout)findViewById(R.id.mycartlayout);
        mycart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,ActivityMy_Cart.class));
            }
        });
        myorder=(LinearLayout)findViewById(R.id.myorderlayout);
        myorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,MyorderActivity.class));
            }
        });
        linkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,SocialMediaActivity.class);
                intent.putExtra("action","Linkedin");
                startActivity(intent);
            }
        });

        twitter=(LinearLayout)findViewById(R.id.twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,SocialMediaActivity.class);
                intent.putExtra("action","Twitter");
                startActivity(intent);
            }
        });
        instagram=(LinearLayout)findViewById(R.id.instagram);
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,SocialMediaActivity.class);
                intent.putExtra("action","Instagram");
                startActivity(intent);
            }
        });
        youtube=(LinearLayout)findViewById(R.id.youtube);
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,SocialMediaActivity.class);
                intent.putExtra("action","Youtube");
                startActivity(intent);
            }
        });
        facebooklayout=(LinearLayout)findViewById(R.id.facebooklayout);
        facebooklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,SocialMediaActivity.class);
                intent.putExtra("action","Facebook");
                startActivity(intent);
                            }
        });
        myaccount=(LinearLayout)findViewById(R.id.myaccountlayout);
        myaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,FragmentActivity.class));
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FragmentActivity.class));
            }
        });
        monthlypackage = (LinearLayout) findViewById(R.id.monthlypackagelayout);
        catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CatalogueActivity.class));
            }
        });
        searchtoys = (CardView) findViewById(R.id.searchtoys);
        searchtoys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, GalleryActivity.class));
            }
        });
        callback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CallBackActivity.class));
            }
        });
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, FeedbackActivity.class));

            }
        });
        brandslayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CatalogueOptionActivity.class);
                intent.putExtra("value", "brands");
                startActivity(intent);
            }
        });
        agelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CatalogueOptionActivity.class);
                intent.putExtra("value", "age");
                startActivity(intent);
            }
        });
        pointlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CatalogueOptionActivity.class);
                intent.putExtra("value", "points");
                startActivity(intent);
            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
            }
        });
        howitworks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HowItWorksActivities.class));
            }
        });
        suggesttoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SuggestToyActivity.class));
            }
        });
        monthlypackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, PackagesActivity.class));
            }
        });
    }

    // Newly item list

    public void newlyImage() {
        JsonArrayRequest newlyReq = new JsonArrayRequest(URLs.NEWLY_TOY_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        newlyItemList = new ArrayList<>();
                        for (int i = 0; i < response.length(); i++) {

                            try {

                                JSONObject obj = response.getJSONObject(i);

                                Favourites favourites = new Favourites();
                                favourites.setId(obj.getString("productId"));
                                favourites.setImage(URLs.NEWLY_TOY_PATH + obj.getString("productPhoto"));
                                favourites.setName(obj.getString("productName"));
                                favourites.setNumber(obj.getString("productPoints"));
                               // favourites.setStock(obj.getString("productStatus"));
                                newlyItemList.add(favourites);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        FavouritesAdapter favouritesAdapter = new FavouritesAdapter(MainActivity.this, newlyItemList,counter);
                        newlyRecyclerView.setLayoutManager(layoutManager2);
                        newlyRecyclerView.setAdapter(favouritesAdapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";

                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }

        });

        // Adding request to request queue
        int socketTimeout = 3000; //  seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        newlyReq.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(newlyReq);
    }

     // favourite toys list

    public void favouriteItem() {
        JsonArrayRequest favouriteReq = new JsonArrayRequest(URLs.FAVOURITE_TOY_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        favouritesItemList = new ArrayList<>();
                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {

                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Favourites favourites = new Favourites();
                                favourites.setId(obj.getString("productId"));
                                favourites.setImage(URLs.FAVOURITE_TOY_PATH + obj.getString("productPhoto"));
                                favourites.setName(obj.getString("productName"));
                                favourites.setNumber(obj.getString("productPoints"));
                               // favourites.setStock(obj.getString("productStatus"));
                                favouritesItemList.add(favourites);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        FavouritesAdapter favouritesAdapter = new FavouritesAdapter(MainActivity.this, favouritesItemList,counter);
                        favouritesRecyclerView.setItemAnimator(new DefaultItemAnimator());
                        favouritesRecyclerView.setLayoutManager(layoutManager);

                        favouritesRecyclerView.setAdapter(favouritesAdapter);
                        favouritesAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";

                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        int socketTimeout = 3000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        favouriteReq.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(favouriteReq);

    }

    // get total number of item from cart

    public void getCartTotalItem()
    {
        SessionManager sessionManager=new SessionManager(MainActivity.this);
        final String userId=sessionManager.getUserid();
        if(!userId.isEmpty()) {
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String totalitem = response.trim();
                    n = Integer.parseInt(totalitem);
                    if(n!=0) {
                        txtViewCount.setVisibility(View.VISIBLE);
                        txtViewCount.setText(totalitem);
                        invalidateOptionsMenu();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if( error instanceof NetworkError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof AuthFailureError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof ServerError) {
                        errormsg="The server could not be found. Please try again after some time!!";
                    }
                    else if (error instanceof ParseError) {
                        errormsg="Parsing error! Please try again after some time!!";
                    }
                    else if (error instanceof NoConnectionError) {
                        errormsg= "Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof TimeoutError) {
                        errormsg = "Connection TimeOut! Please check your internet connection.";
                    }
                    Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", userId);
                    params.put("action", "total_cart_item");
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartTotalItem();
    }
   public void exitapp() {
       LayoutInflater layoutInflater = LayoutInflater.from(this);
       View view = layoutInflater.inflate(R.layout.app_exit_layout, null);
       final AlertDialog.Builder builder = new AlertDialog.Builder(this);
       builder.setView(view);
       Button cancel = (Button) view.findViewById(R.id.cancel);
       Button ok = (Button) view.findViewById(R.id.ok);
       final AlertDialog alertDialog = builder.create();
       alertDialog.show();
       cancel.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               alertDialog.dismiss();
           }
       });
       ok.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });

   }
   public void getAllCategoryData()
   {
       homecategorylist=new ArrayList<>();
       VolleyResponse volleyResponse=new VolleyResponse() {
           @Override
           public void notifySuccess(int requestType, final JSONArray response) {
               for (int i = 0; i<10; i++) {
                   try {
                       JSONObject obj = response.getJSONObject(i);
                       Favourites favourites=new Favourites();
                       favourites.setId(obj.getString("productId"));
                       favourites.setImage(URLs.SEARCH_IMAGE_PATH+obj.getString("productPhoto"));
                       favourites.setName(obj.getString("productName"));
                       favourites.setPoints(obj.getString("productPoints"));
                       favourites.setStock(obj.getString("productStatus"));
                       homecategorylist.add(favourites);
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }

               }
               // notifying list adapter about data changes
               final AllCategoryAdapter catalogueAdapter = new AllCategoryAdapter(allcategoryRecyclerview,MainActivity.this,homecategorylist,counter);
               allcategoryRecyclerview.setAdapter(catalogueAdapter);
               catalogueAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                   @Override
                   public void onLoadMore() {
                       homecategorylist.add(null);
                       catalogueAdapter.notifyItemInserted(homecategorylist.size()-1);
                       new Handler().postDelayed(new Runnable() {
                           @Override
                           public void run() {
                               homecategorylist.remove(homecategorylist.size()-1);
                               catalogueAdapter.notifyItemRemoved(homecategorylist.size());
                               int index=homecategorylist.size();
                               int end=index+10;
                               for(int i=index;i<end;i++) {
                                   try {

                                       JSONObject obj = response.getJSONObject(i);

                                       Favourites favourites = new Favourites();
                                       favourites.setId(obj.getString("productId"));
                                       favourites.setImage(URLs.SEARCH_IMAGE_PATH + obj.getString("productPhoto"));
                                       favourites.setName(obj.getString("productName"));
                                       favourites.setPoints(obj.getString("productPoints"));
                                       favourites.setStock(obj.getString("productStatus"));
                                       homecategorylist.add(favourites);
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }

                               }
                               catalogueAdapter.setLoaded();
                           }
                       },4000);
                   }
               });
           }

           @Override
           public void notifyError(int requestType, VolleyError error) {

           }
       };
       VolleyService volleyService=new VolleyService(volleyResponse,this);
       JSONArray sendObj = null;

       try {
           sendObj = new JSONArray("{'null':'null'}");
       } catch (JSONException e) {
           e.printStackTrace();
       }
       volleyService.postDataVolley(Request.Method.POST,URLs.HOMECATEGORY_URL,sendObj);

   }
}