package com.toybraryapp.childs.toybrary.subCategoryList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.toybraryapp.childs.toybrary.activities.CategoryHolder;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.activities.SubCategoryItemViewActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Dharmender on 01-11-2017.
 */

public class ShowCategoryAdapeter extends BaseExpandableListAdapter {
    private Context content;
    private ArrayList<CategoryHolder> headerlist;
    private HashMap<String,ArrayList<String>> childlist;
    private ExpandableListView listView;

    TextView headerTextview,categoryid;
    ImageView headerImageview;
    public ShowCategoryAdapeter(Context content, ArrayList<CategoryHolder> headerlist, HashMap<String,ArrayList<String>> childlist, ExpandableListView listView)
    {
        this.content = content;
        this.headerlist=headerlist;
        this.childlist=childlist;
        this.listView=listView;
    }
    @Override
    public int getGroupCount() {
        return headerlist.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childlist.get(headerlist.get(groupPosition).getPARENTCATEGORY()).size();
    }

    @Override
    public Object getGroup(int position) {
        return  headerlist.get(position);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childlist.get(headerlist.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childePosition) {
        return childePosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater layoutInflater= (LayoutInflater) content.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=layoutInflater.inflate(R.layout.simple_list,null);
        }
        headerTextview=(TextView) convertView.findViewById(R.id.parentcategory);
        headerImageview= (ImageView) convertView.findViewById(R.id.parentcategoryarrow);
       categoryid=(TextView) convertView.findViewById(R.id.catId);
        final CheckBox checkBox=(CheckBox)convertView.findViewById(R.id.cbox);

       categoryid.setText(headerlist.get(groupPosition).getParentId());
        headerTextview.setText(headerlist.get(groupPosition).getPARENTCATEGORY());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                String id=headerlist.get(groupPosition).getParentId();
                String category=headerlist.get(groupPosition).getPARENTCATEGORY();
                Intent intent=new Intent(content, SubCategoryItemViewActivity.class);
                intent.putExtra("get",category);
                intent.putExtra("id",id);
                content.startActivity(intent);
                checkBox.setChecked(false);
            }
        });
        if(isExpanded)
        {
            headerImageview.setImageResource(R.mipmap.ic_action_collapse);
        }
        else
        {
            headerImageview.setImageResource(R.mipmap.ic_action_expand);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup viewGroup) {
        if(convertView==null){
            LayoutInflater layoutInflater= (LayoutInflater) content.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=layoutInflater.inflate(R.layout.singlechildcategory,null);
        }
        TextView headerTextview= (TextView) convertView.findViewById(R.id.childcategory);
        headerTextview.setText(childlist.get(headerlist.get(groupPosition).getPARENTCATEGORY()).get(childPosition));
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
