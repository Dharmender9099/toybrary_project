package com.toybraryapp.childs.toybrary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OrderDetailsActivity extends AppCompatActivity {
TextView orderid,name,points,quantity,shippingaddress,landmark,shippingcity,shippingpostcode,shippingcountry,review;
String orderId,errormsg,productid;
NetworkImageView productimage;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        getSupportActionBar().setTitle("Order Details");
        orderid=(TextView)findViewById(R.id.orderid);
        name=(TextView)findViewById(R.id.name);
        points=(TextView)findViewById(R.id.points);
        quantity=(TextView)findViewById(R.id.quantity);
        shippingaddress=(TextView)findViewById(R.id.shipping_address);
        landmark=(TextView)findViewById(R.id.landmark);
        shippingcity=(TextView)findViewById(R.id.shipping_city);
        shippingpostcode=(TextView)findViewById(R.id.shipping_postcode);
        shippingcountry=(TextView)findViewById(R.id.shipping_country);
        productimage=(NetworkImageView) findViewById(R.id.productimage);
        orderId=getIntent().getStringExtra("orderid");
        productid=getIntent().getStringExtra("productid");
        orderdetails();
        review=(TextView)findViewById(R.id.review);
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(OrderDetailsActivity.this,ReviewActivity.class);
                intent.putExtra("productid",productid);
                startActivity(intent);
            }
        });
    }
    public void orderdetails()
    {
        StringRequest orderrequest=new StringRequest(Request.Method.POST, URLs.ORDERDETAILS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("orderdetails");
                    for(int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject obj= jsonArray.getJSONObject(i);
                        orderid.setText(obj.getString("orderid"));
                        shippingaddress.setText(obj.getString("shipping_address"));
                        landmark.setText(obj.getString("landmark"));
                        shippingcity.setText(obj.getString("shipping_city"));
                        shippingpostcode.setText(obj.getString("shipping_postcode"));
                        shippingcountry.setText(obj.getString("shipping_country"));
                        productimage.setImageUrl(URLs.SEARCH_IMAGE_PATH+obj.get("productImage"),imageLoader);
                        points.setText(obj.getString("points"));
                        quantity.setText(obj.getString("quantity"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map=new HashMap<>();
                map.put("orderid",orderId);
                return map;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(orderrequest);
    }
}
