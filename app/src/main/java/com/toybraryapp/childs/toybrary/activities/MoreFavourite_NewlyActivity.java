package com.toybraryapp.childs.toybrary.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.adapter.GridAdapter;
import com.toybraryapp.childs.toybrary.model.Favourites;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.Counter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MoreFavourite_NewlyActivity extends AppCompatActivity {
    private String getvalue;
    TextView txtViewCount;
   private List<Favourites> moreItemList;
   RecyclerView moreItemRecyclerview;
    GridLayoutManager manager;
    Counter counter;
    String errormsg="";
    int n;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_favourite__newly);

        counter=new Counter() {
            @Override
            public void setCounter(int count) {
                n=n+count;
                invalidateOptionsMenu();
            }
        };
        getvalue=getIntent().getStringExtra("getvalue");

        if(getvalue.equals("favourites"))
        {
            getSupportActionBar().setTitle("Favourites Toys");
         getMoreProduct(URLs.FAVOURITE_TOY_URL,URLs.FAVOURITE_TOY_PATH);

        }
        else
        {
            getSupportActionBar().setTitle("Newly Arrived Toys");
          getMoreProduct(URLs.NEWLY_TOY_URL,URLs.NEWLY_TOY_PATH);
        }
        moreItemRecyclerview=(RecyclerView)findViewById(R.id.moreItemRecyclerview);
        manager = new GridLayoutManager(this, 2);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.addcart);
        MenuItemCompat.setActionView(menuItem, R.layout.cart_icon_toolbar);
        RelativeLayout mycarttoolbar = (RelativeLayout) MenuItemCompat.getActionView(menuItem);
        RelativeLayout relativeLayout=(RelativeLayout)mycarttoolbar.findViewById(R.id.relative_layout_item_count);
        txtViewCount = (TextView) mycarttoolbar.findViewById(R.id.badge_notification_1);
        if(n!=0) {
            txtViewCount.setVisibility(View.VISIBLE);
            txtViewCount.setText(String.valueOf(n));
        }
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MoreFavourite_NewlyActivity.this, ActivityMy_Cart.class));
            }
        });
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.contactus) {
            startActivity(new Intent(MoreFavourite_NewlyActivity.this, ContactUsActivity.class));
        } else if (id == R.id.logout) {
            SharedPreferences preferences =getSharedPreferences("myPref1",MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
            finish();
        }
        else if(id==R.id.editprofile)
        {
            startActivity(new Intent(this,EditProfileActivity.class));
        }
        else if(id==R.id.changepass)
        {
            startActivity(new Intent(this,ChangePasswordActivity.class));
        }
        else if(id==R.id.privacy)
        {
            startActivity(new Intent(this,PolicyActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
    public void getMoreProduct(String PRODUCT_URL, final String TOY_IMAGE_PATH)
    {
        JsonArrayRequest moreItemReq = new JsonArrayRequest(PRODUCT_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        moreItemList = new ArrayList<>();
                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {

                            try {

                                JSONObject obj = response.getJSONObject(i);

                                Favourites moreItem = new Favourites();
                                moreItem.setId(obj.getString("productId"));
                                moreItem.setImage(TOY_IMAGE_PATH+obj.getString("productPhoto"));

                                moreItem.setName(obj.getString("productName"));
                                moreItem.setNumber(obj.getString("productPoints"));
                                moreItem.setStock(obj.getString("productStatus"));
                                moreItemList.add(moreItem);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        GridAdapter moreItemAdapter = new GridAdapter(MoreFavourite_NewlyActivity.this, moreItemList,counter);
                        moreItemRecyclerview.setItemAnimator(new DefaultItemAnimator());
                        moreItemRecyclerview.setLayoutManager(manager);
                        moreItemRecyclerview.setAdapter(moreItemAdapter);
                        moreItemAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();

            }
        });

        // Adding request to request queue
        VolleySingleton.getInstance(this).addToRequestQueue(moreItemReq);
    }
    public void getCartTotalItem()
    {
        SessionManager sessionManager=new SessionManager(MoreFavourite_NewlyActivity.this);
        final String userId=sessionManager.getUserid();
        if(!userId.isEmpty()) {
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String totalitem = response.trim();
                    n = Integer.parseInt(totalitem);
                    if(n!=0) {
                        txtViewCount.setVisibility(View.VISIBLE);
                        txtViewCount.setText(totalitem);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if( error instanceof NetworkError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof AuthFailureError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof ServerError) {
                        errormsg="The server could not be found. Please try again after some time!!";
                    }
                    else if (error instanceof ParseError) {
                        errormsg="Parsing error! Please try again after some time!!";
                    }
                    else if (error instanceof NoConnectionError) {
                        errormsg = "Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof TimeoutError) {
                        errormsg = "Connection TimeOut! Please check your internet connection.";
                    }
                    Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", userId);
                    params.put("action", "total_cart_item");
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        getCartTotalItem();
        invalidateOptionsMenu();
    }
}
