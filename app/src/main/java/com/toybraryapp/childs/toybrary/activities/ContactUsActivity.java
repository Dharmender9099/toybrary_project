package com.toybraryapp.childs.toybrary.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.captcha.TextCaptcha;
import com.toybraryapp.childs.toybrary.url.URLs;

import java.util.HashMap;
import java.util.Map;

public class ContactUsActivity extends AppCompatActivity {
    EditText name,email,mobile,message,captcha;
    ImageView showcaptcha;
    Button send;
    String errormsg="";
    private ProgressDialog progressDialog;
    private static final String LOG_TAG = ContactUsActivity.class.getSimpleName();
    String maincaptcha="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        name=(EditText)findViewById(R.id.name);
        email=(EditText)findViewById(R.id.email);
        mobile=(EditText) findViewById(R.id.mobile);
        message=(EditText)findViewById(R.id.message);
        captcha=(EditText)findViewById(R.id.captcha);
        showcaptcha=(ImageView)findViewById(R.id.showcaptcha);
        send=(Button)findViewById(R.id.send);
        generateCaptcha();
        maincaptcha=String.valueOf(TextCaptcha.cap);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                   contactUs();
            }
        });

    }
    private void generateCaptcha() {
        TextCaptcha textCaptcha=new TextCaptcha(600,150,4,TextCaptcha.TextOptions.LETTERS_ONLY);
        showcaptcha.setImageBitmap(textCaptcha.getImage());
    }

    private void contactUs()
    {
        final String user_name=name.getText().toString();
        final String user_email=email.getText().toString();
        final String user_mobile=mobile.getText().toString();
        final String user_message=message.getText().toString();
        final String inputcaptcha=captcha.getText().toString();
        if(!inputcaptcha.matches(maincaptcha))
        {
            Toast.makeText(getApplication(),maincaptcha,Toast.LENGTH_LONG).show();
           captcha.setError("please enter valid captcha");
           captcha.requestFocus();
           return;
        }
        if(user_name.isEmpty())
        {
            name.setError("please enter name");
            name.requestFocus();
            return;
        }
        if(user_email.isEmpty())
        {
            email.setError("please enter mail-id");
            email.requestFocus();
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(user_email).matches()) {
            email.setError("Enter a valid email");
            email.requestFocus();
            return;
        }

        if(user_mobile.isEmpty())
        {
            mobile.setError("please enter mobile");
            mobile.requestFocus();
            return;
        }

        if(user_mobile.length()<10)
        {
            mobile.setError("please enter 10 digit number");
            mobile.requestFocus();
            return;
        }
        if(user_mobile.length()>10)
        {
            mobile.setError("please enter valied moible no");
            mobile.requestFocus();
            return;
        }
        if(user_message.isEmpty())
        {
            message.setError("please enter any query");
            message.requestFocus();
            return;
        }
        progressDialog = ProgressDialog.show(ContactUsActivity.this, "", "Please wait...");
        StringRequest stringRequest=new StringRequest(StringRequest.Method.POST, URLs.CONTACTUS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
            String res=response.trim();
            if(res.equals("Thanks for contact us.!"))
            {
                Toast.makeText(ContactUsActivity.this,res,Toast.LENGTH_LONG).show();
                startActivity(new Intent(ContactUsActivity.this,MainActivity.class));
            }
            else
            {
                Toast.makeText(ContactUsActivity.this,res,Toast.LENGTH_LONG).show();
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("name",user_name);
                params.put("email",user_email);
                params.put("mobile",user_mobile);
                params.put("message",user_message);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}
