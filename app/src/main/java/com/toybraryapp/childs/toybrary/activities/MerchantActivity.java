package com.toybraryapp.childs.toybrary.activities;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.paytm.Api;
import com.toybraryapp.childs.toybrary.paytm.Checksum;
import com.toybraryapp.childs.toybrary.paytm.Constants;
import com.toybraryapp.childs.toybrary.paytm.Paytm_user;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MerchantActivity extends AppCompatActivity implements PaytmPaymentTransactionCallback {
    public static  String txnAmount="";
   // String txnAmount="1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        generateCheckSum();
    }

    private void generateCheckSum() {

        //getting the tax amount first.

        //creating a retrofit object.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //creating the retrofit api service
        Api apiService = retrofit.create(Api.class);

        //creating paytm object
        //containing all the values required
        final Paytm_user paytm_user = new Paytm_user(
                Constants.M_ID,
                Constants.CHANNEL_ID,
                txnAmount,
                Constants.WEBSITE,
                Constants.CALLBACK_URL,
                Constants.INDUSTRY_TYPE_ID
        );

        //creating a call object from the apiService
        Call<Checksum> call = apiService.getChecksum(
                paytm_user.getmId(),
                paytm_user.getOrderId(),
                paytm_user.getCustId(),
                paytm_user.getChannelId(),
                paytm_user.getTxnAmount(),
                paytm_user.getWebsite(),
                paytm_user.getCallBackUrl(),
                paytm_user.getIndustryTypeId()
        );

        //making the call to generate checksum
        call.enqueue(new Callback<Checksum>() {
            @Override
            public void onResponse(Call<Checksum> call, Response<Checksum> response) {

                //once we get the checksum we will initiailize the payment.
                //the method is taking the checksum we got and the paytm object as the parameter
                initializePaytmPayment(response.body().getChecksumHash(), paytm_user);
            }

            @Override
            public void onFailure(Call<Checksum> call, Throwable t) {

            }
        });
    }

    private void initializePaytmPayment(String checksumHash, Paytm_user paytm_us) {

        //getting paytm service
       // PaytmPGService Service = PaytmPGService.getStagingService();

        //use this when using for production
        PaytmPGService Service = PaytmPGService.getProductionService();

        //creating a hashmap and adding all the values required
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("MID", Constants.M_ID);
        paramMap.put("ORDER_ID", paytm_us.getOrderId());
        paramMap.put("CUST_ID", paytm_us.getCustId());
        paramMap.put("CHANNEL_ID", paytm_us.getChannelId());
        paramMap.put("TXN_AMOUNT", paytm_us.getTxnAmount());
        paramMap.put("WEBSITE", paytm_us.getWebsite());
        paramMap.put("CALLBACK_URL", paytm_us.getCallBackUrl());
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("INDUSTRY_TYPE_ID", paytm_us.getIndustryTypeId());


        //creating a paytm order object using the hashmap
        PaytmOrder order = new PaytmOrder(paramMap);

        //intializing the paytm service
        Service.initialize(order, null);

        //finally starting the payment transaction
        Service.startPaymentTransaction(this, true, true, this);

    }
        @Override
        public void onTransactionResponse (Bundle bundle){
            Log.i("response",bundle.toString());
            String txnamount=bundle.getString("TXNAMOUNT");
            String txnid=bundle.getString("TXNID");
            String status=bundle.getString("STATUS");
            String txndate=bundle.getString("TXNDATE");
            if(status.equals("TXN_SUCCESS"))
            {
                Intent intent=new Intent(MerchantActivity.this,ThankYouActivity.class);
                intent.putExtra("txnid",txnid);
                intent.putExtra("txndate",txndate);
                intent.putExtra("txnamount",txnamount);
                startActivity(intent);
            }
            Toast.makeText(getApplication(),txnamount,Toast.LENGTH_LONG).show();

        }

        @Override
        public void networkNotAvailable () {
            Toast.makeText(this, "Network error", Toast.LENGTH_LONG).show();
        }

        @Override
        public void clientAuthenticationFailed (String s){
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        }

        @Override
        public void someUIErrorOccurred (String s){
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onErrorLoadingWebPage ( int i, String s, String s1){
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onBackPressedCancelTransaction () {
            Toast.makeText(this, "Back Pressed", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onTransactionCancel (String s, Bundle bundle){
            Toast.makeText(this, s + bundle.toString(), Toast.LENGTH_LONG).show();
        }
}