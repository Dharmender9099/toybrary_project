package com.toybraryapp.childs.toybrary.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.Gallery;
import com.toybraryapp.childs.toybrary.model.SessionManager;
import com.toybraryapp.childs.toybrary.model.VolleySingleton;
import com.toybraryapp.childs.toybrary.url.URLs;
import com.toybraryapp.childs.toybrary.util.Counter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SingleProductActivity extends AppCompatActivity {
TextView points,age,brands,addtocart,mcontinue,description,title;
    NetworkImageView productImage,productImage1,productImage2,productImage3,productImage4;
    ProgressBar progressbar;
    String New_url,userId;
    Counter counter;
    TextView txtViewCount;
    int n;
    String val="";
    String errormsg="";
     ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_product);
        points=(TextView)findViewById(R.id.points);
        title=(TextView)findViewById(R.id.title);
        age=(TextView)findViewById(R.id.age);
        brands=(TextView)findViewById(R.id.brands);
        progressbar=(ProgressBar)findViewById(R.id.progressbar);
        productImage=(NetworkImageView)findViewById(R.id.productImage);
        productImage1=(NetworkImageView)findViewById(R.id.productImage1);
        productImage2=(NetworkImageView)findViewById(R.id.productImage2);
        productImage3=(NetworkImageView)findViewById(R.id.productImage3);
        productImage4=(NetworkImageView)findViewById(R.id.productImage4);
        description=(TextView)findViewById(R.id.description);
        addtocart=(TextView)findViewById(R.id.addtocart);
        mcontinue=(TextView)findViewById(R.id.mcontinue);
        val=getIntent().getStringExtra("productId");
        New_url= URLs.PRODUCT_DETAILS+val;
        getItemDetails();
        counter=new Counter() {
            @Override
            public void setCounter(int count) {
                n=n+count;
                invalidateOptionsMenu();
            }
        };
        mcontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // startActivity(new Intent(SingleProductActivity.this,PackagesActivity.class));
            }
        });
        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SessionManager sessionManager=new SessionManager(SingleProductActivity.this);
                 userId=sessionManager.getUserid();
                String add = "additem";
                if(!userId.isEmpty()) {
                    updateCart(val, userId, add);
                }
                else {
                    Toast.makeText(getApplicationContext(),"please register first!",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.addcart);
        MenuItemCompat.setActionView(menuItem, R.layout.cart_icon_toolbar);
        RelativeLayout mycarttoolbar = (RelativeLayout) MenuItemCompat.getActionView(menuItem);
        RelativeLayout relativeLayout=(RelativeLayout)mycarttoolbar.findViewById(R.id.relative_layout_item_count);
        txtViewCount = (TextView) mycarttoolbar.findViewById(R.id.badge_notification_1);
        if(n!=0) {
            txtViewCount.setVisibility(View.VISIBLE);
            txtViewCount.setText(String.valueOf(n));
        }
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SingleProductActivity.this, ActivityMy_Cart.class));
            }
        });
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.contactus) {
            startActivity(new Intent(SingleProductActivity.this, ContactUsActivity.class));
        } else if (id == R.id.logout) {
            SharedPreferences preferences =getSharedPreferences("myPref1",MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
            finish();
        }
        else if(id==R.id.editprofile)
        {
            startActivity(new Intent(this,EditProfileActivity.class));
        }
        else if(id==R.id.changepass)
        {
            startActivity(new Intent(this,ChangePasswordActivity.class));
        }
        else if(id==R.id.privacy)
        {
            startActivity(new Intent(this,PolicyActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
    public void getItemDetails()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,New_url,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray packages=jsonObject.getJSONArray("products_detail");
                    for(int i=0;i<packages.length();i++) {

                        JSONObject obj=packages.getJSONObject(i);
                        age.setText(obj.getString("productAge"));
                        title.setText(obj.getString("productName"));
                        points.setText(obj.getString("productPoints"));

                        brands.setText(obj.getString("productBrand"));
                        String prodes=obj.getString("productDescription");
                        prodes=prodes.replaceAll("\\<.*?\\>", "");
                       // description.setText(obj.getString("productDescription"));
                        description.setText(prodes);
                    }

                    JSONArray proImage=jsonObject.getJSONArray("products_image");
                    Gallery gallery=new Gallery();
                    for(int i=0;i<proImage.length();i++) {
                        JSONObject obj =proImage.getJSONObject(i);
                       if(i==0)
                       {
                           productImage.setImageUrl(URLs.SEARCH_IMAGE_PATH+obj.get("productPhoto"),imageLoader);
                       }
                        if(i==1)
                        {
                            productImage1.setVisibility(View.VISIBLE);
                            productImage1.setImageUrl(URLs.SEARCH_IMAGE_PATH+obj.get("productPhoto"),imageLoader);
                            final  String img1= String.valueOf(obj.get("productPhoto"));
                            productImage1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    productImage.setImageUrl(URLs.SEARCH_IMAGE_PATH+img1,imageLoader);
                                }
                            });
                        }
                        if(i==2)
                        {
                            productImage2.setVisibility(View.VISIBLE);
                            productImage2.setImageUrl(URLs.SEARCH_IMAGE_PATH+obj.get("productPhoto"),imageLoader);
                            final  String img2= String.valueOf(obj.get("productPhoto"));
                            productImage2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    productImage.setImageUrl(URLs.SEARCH_IMAGE_PATH+img2,imageLoader);
                                }
                            });
                        }
                        if(i==3)
                        {
                            productImage3.setVisibility(View.VISIBLE);
                            productImage3.setImageUrl(URLs.SEARCH_IMAGE_PATH+obj.get("productPhoto"),imageLoader);
                            final  String img3= String.valueOf(obj.get("productPhoto"));
                            productImage3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    productImage.setImageUrl(URLs.SEARCH_IMAGE_PATH+img3,imageLoader);
                                }
                            });
                        }
                        if(i==4)
                        {
                            productImage4.setVisibility(View.VISIBLE);
                            productImage4.setImageUrl(URLs.SEARCH_IMAGE_PATH+obj.get("productPhoto"),imageLoader);
                            final  String img4= String.valueOf(obj.get("productPhoto"));
                            productImage4.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    productImage.setImageUrl(URLs.SEARCH_IMAGE_PATH+img4,imageLoader);
                                }
                            });
                        }
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

            }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if( error instanceof NetworkError) {
                            errormsg="Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof AuthFailureError) {
                            errormsg="Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof ServerError) {
                            errormsg="The server could not be found. Please try again after some time!!";
                        }
                        else if (error instanceof ParseError) {
                            errormsg="Parsing error! Please try again after some time!!";
                        }
                        else if (error instanceof NoConnectionError) {
                            errormsg = "Cannot connect to Internet...Please check your connection!";
                        }
                        else if (error instanceof TimeoutError) {
                            errormsg = "Connection TimeOut! Please check your internet connection.";
                        }
                        Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
                    }
                });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }
    public void getCartTotalItem()
    {
        SessionManager sessionManager=new SessionManager(SingleProductActivity.this);
        final String userId=sessionManager.getUserid();
        if(!userId.isEmpty()) {
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String totalitem = response.trim();
                    n = Integer.parseInt(totalitem);
                    if(n!=0) {
                        txtViewCount.setVisibility(View.VISIBLE);
                        txtViewCount.setText(totalitem);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if( error instanceof NetworkError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof AuthFailureError) {
                        errormsg="Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof ServerError) {
                        errormsg="The server could not be found. Please try again after some time!!";
                    }
                    else if (error instanceof ParseError) {
                        errormsg="Parsing error! Please try again after some time!!";
                    }
                    else if (error instanceof NoConnectionError) {
                        errormsg = "Cannot connect to Internet...Please check your connection!";
                    }
                    else if (error instanceof TimeoutError) {
                        errormsg = "Connection TimeOut! Please check your internet connection.";
                    }
                    Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", userId);
                    params.put("action", "total_cart_item");
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        getCartTotalItem();
        invalidateOptionsMenu();
    }
    public void updateCart(final String productid, final String userid,final String additem)
    {
        StringRequest stringRequest=new StringRequest(Request.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res=response.trim();
                Toast.makeText(getApplication(),res,Toast.LENGTH_LONG).show();
                if(res.equals("product successfully added in cart")) {
                    n=n+1;
                    txtViewCount.setText(String.valueOf(n));
                    invalidateOptionsMenu();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( error instanceof NetworkError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof AuthFailureError) {
                    errormsg="Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof ServerError) {
                    errormsg="The server could not be found. Please try again after some time!!";
                }
                else if (error instanceof ParseError) {
                    errormsg="Parsing error! Please try again after some time!!";
                }
                else if (error instanceof NoConnectionError) {
                    errormsg = "Cannot connect to Internet...Please check your connection!";
                }
                else if (error instanceof TimeoutError) {
                    errormsg = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getApplication(),errormsg,Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("product_id",productid);
                params.put("user_id",userid);
                params.put("action",additem);
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void continueItem()
    {
        StringRequest itemReq=new StringRequest(StringRequest.Method.POST, URLs.UPDATE_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            String res=response.trim();
            if(res.equals("product successfully added in cart"))
            {
            Intent intent=new Intent(SingleProductActivity.this,CheckOutActivity.class);
                intent.putExtra("action","continue");
            intent.putExtra("cart_totalpoint",points.getText().toString());
            startActivity(intent);
            }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("userid", userId);
                map.put("product_id",val);
                return map;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(itemReq);
    }
}
