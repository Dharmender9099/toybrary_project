package com.toybraryapp.childs.toybrary.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.toybraryapp.childs.toybrary.R;
import com.toybraryapp.childs.toybrary.model.Gallery;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Dharmender on 15-01-2018.
 */

public class MyPackageAdapter extends RecyclerView.Adapter<MyPackageAdapter.MyPackageHolder>{
    Context context;
    List<Gallery> list=new ArrayList<>();
    public MyPackageAdapter(Context context,List<Gallery> list)
    {
        this.context=context;
        this.list=list;
    }
    @Override
    public MyPackageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.package_layout,parent,false);
        return new MyPackageAdapter.MyPackageHolder(view);
    }

    @Override
    public void onBindViewHolder(MyPackageHolder holder, int position) {
        Gallery gallery=list.get(position);
        holder.packagename.setText(gallery.getName());
        String pname=gallery.getName();
        holder.packageprice.setText(gallery.getPrice());
        holder.packagevalidity.setText(gallery.getValidity());
        holder.packagepoints.setText(gallery.getPoints());
        holder.packagestatus.setText(gallery.getStatus());
        String stus=gallery.getStatus();
        holder.package_exp_date.setText(gallery.getExpiredate());
        holder.purchasedate.setText(gallery.getPurchasedate());
        holder.fname.setText(String.valueOf(pname.charAt(0)));

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        GradientDrawable bgShape = (GradientDrawable)holder.fname.getBackground();
        bgShape.setColor(color);
        if(stus.equals("Expire"))
        {
            GradientDrawable bgShape1 = (GradientDrawable)holder.status.getBackground();
            bgShape1.setColor(Color.GRAY);
        }
        else
        {
            GradientDrawable bgShape1 = (GradientDrawable)holder.status.getBackground();
            bgShape1.setColor(Color.GREEN);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyPackageHolder extends RecyclerView.ViewHolder
    {
     TextView packagename,packageprice,packagevalidity,packagestatus,packagepoints,package_exp_date,fname,status,purchasedate;

        public MyPackageHolder(View itemView) {
            super(itemView);
            packagename=(TextView)itemView.findViewById(R.id.packagename);
            packagevalidity=(TextView)itemView.findViewById(R.id.packagevalidity);
            packageprice=(TextView)itemView.findViewById(R.id.packageprice);
            packagestatus=(TextView)itemView.findViewById(R.id.packagestatus);
            packagepoints=(TextView)itemView.findViewById(R.id.packagepoints);
            package_exp_date=(TextView)itemView.findViewById(R.id.packageexpiredate);
            fname=(TextView)itemView.findViewById(R.id.firstchar);
            status=(TextView)itemView.findViewById(R.id.status);
            purchasedate=(TextView)itemView.findViewById(R.id.purchasedate);
        }
    }
}
